package com.mymapmanager

class AttachmentTagLib {

	def springSecurityService
   
  def showAttachmentsForThisInstance={attrs, body ->
  	def currentUser = springSecurityService.currentUser
  	def attachmentLink = AttachmentLink.findWhere(
  		type:attrs.instance.class.getSimpleName(),
      attachmentRef:attrs.instance.id)
		out << render(template:"/attachment/showAttachmentsForThisInstance",
      model:[attachmentLink:attachmentLink])
  }

  def addAttachmentsForThisInstance={attrs, body ->
  	def instanceClazz= attrs.instance.class.name
  	def instanceId=attrs.instance.id
  	def attachmentLink = AttachmentLink.findWhere(
  		type:attrs.instance.class.getSimpleName(),
      attachmentRef:attrs.instance.id)
		out << render(template:"/attachment/addAttachmentsForThisInstance",
      model:[instanceId:instanceId,
      			instanceClazz:instanceClazz,
      			attachmentLink:attachmentLink])
  }

}
