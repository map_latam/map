package com.mymapmanager

import grails.transaction.Transactional

@Transactional
class TopicService {

  def collaborationService

  def create(topicCommand) {
    def topic = new Topic(title:topicCommand.title)
    def meeting = Meeting.get(topicCommand.idMeeting)
    meeting.addToTopics(topic)
    meeting.save()
    topic
  }

  // TODO : Refactor para diseñar a una sola clase todos las creaciones
  // de elementos que implementen Collaboration
  def update(String username, topicUpdateCommand) {
    def user = User.findByUsername(username)

    def topic = Topic.get(topicUpdateCommand.idTopic)
    topic.title = topicUpdateCommand.title
    topic.duration = topicUpdateCommand.duration
    topic.arrange = topicUpdateCommand.arrange
    topic.description = topicUpdateCommand.description
    topic.save()

    collaborationService.inviteParticipantToCollaborateToThisInstance(topic, user, topicUpdateCommand.participants)

    topic
  }


}