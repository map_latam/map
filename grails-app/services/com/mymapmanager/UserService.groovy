package com.mymapmanager

import grails.transaction.Transactional

@Transactional
class UserService {

  def createUserWithUsernamePasswordAndEnable(String username,String password,enabled = true) {
    User user = User.findByUsername(username)
    if (!user)
      return theCreationOfNewUser(username,password,enabled)
    if(user && user.enabled)
      throw new RuntimeException("The user '$username' already exists...")
    if(user && !user.enabled)
      enableAnExistingUserAndUpdatingPassword(user, password)
    user
  }

  private User theCreationOfNewUser(String username, String password, boolean enabled){
    User user = new User(username:username,password:password,enabled:enabled).save()
    Role roleUser = Role.findByAuthority("ROLE_USER")
    UserRole.create user, roleUser
    user
  }

  private void enableAnExistingUserAndUpdatingPassword(User user, password){
    user.enabled =  true
    user.password = password
    user.save()
  }

}