package com.mymapmanager

import grails.transaction.Transactional
import org.grails.s3.S3Asset

@Transactional
class AttachmentService {

	def s3AssetService

	def createAttachmentForThisInstance(instance,file){
		if(!Attachment.class.isAssignableFrom(instance.class))
			throw new Exception("Pelaz!")
		def attachmentLink
		if(AttachmentLink.findWhere(type:instance.class.getSimpleName(),attachmentRef:instance.id))
			attachmentLink = AttachmentLink.findWhere(type:instance.class.getSimpleName(),attachmentRef:instance.id)
		else
			attachmentLink = new AttachmentLink(attachmentRef:instance.id,type:instance.class.getSimpleName())
		S3Asset receipt = new S3Asset()
		File tmp = s3AssetService.getNewTmpLocalFile(file.contentType)
		file.transferTo(tmp)
    receipt.newFile(tmp)
    receipt.mimeType = file.contentType
    receipt.title = file.getOriginalFilename()
    s3AssetService.put(receipt)
		attachmentLink.addToAttachments(receipt)
		attachmentLink.save()
	}

	def deleteAttachFromAttachmentForThisInstance(def attachmentLink, S3Asset s3Asset){
		s3AssetService.delete(s3Asset)
		attachmentLink.removeFromAttachments(s3Asset).save()
		attachmentLink
	}

}
	

