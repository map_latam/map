package com.mymapmanager

import grails.transaction.Transactional

@Transactional
class PrivilegeService {

  def assignPrivilegeToParticipantInInstance(privilege, username, collaborationInstance){
    if(!Collaboration.class.isAssignableFrom(collaborationInstance.class))
      throw new RuntimeException("Class does not implement Collaboration")

    def collaborationLink = CollaborationLink.findByCollaborationRefAndType(
        collaborationInstance.id,
        collaborationInstance.class.getSimpleName())

    def collaborator = collaborationLink.participants.find{
      it.user.username == username
    }

    collaborator.privilege = privilege
    collaborator.save()
    collaborationLink
  }

}