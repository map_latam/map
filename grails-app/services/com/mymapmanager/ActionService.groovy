package com.mymapmanager

import grails.transaction.Transactional

@Transactional
class ActionService {

  def collaborationService

  // TODO : Refactor para diseñar a una sola clase todos las creaciones
  // de elementos que implementen Collaboration
  Action createAction(userId, actionCommand){
    def user = User.get(userId)
    def action = new Action (name:actionCommand.name, 
                            description:actionCommand.description, 
                            startingDay:new Date().parse("dd/MM/yyyy", actionCommand.startingDay ?: new Date().format('dd/MM/yyyy')),
                            finishedDay:new Date().parse("dd/MM/yyyy", actionCommand.finishedDay ?: new Date().format('dd/MM/yyyy')),
                            autor:user)
    action.save()
    collaborationService.inviteParticipantToCollaborateToThisInstance(action, user, actionCommand.participants)
    action
  }

}
