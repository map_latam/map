package com.mymapmanager

import grails.transaction.Transactional
import com.mymapmanager.*

@Transactional
class ProjectService {

  def actionService
  def meetingService
  def collaborationService

  // TODO : Refactor para diseñar a una sola clase todos las creaciones
  // de elementos que implementen Collaboration
  Project createProject(userId, projectCommand){
    def user = User.get(userId)
    def project = new Project (name:projectCommand.name, 
                              description:projectCommand.description, 
                              startingDay:new Date().parse("dd/MM/yyyy", projectCommand.startingDay), 
                              finishedDay:new Date().parse("dd/MM/yyyy", projectCommand.finishedDay),
                              autor:user)
    project.save()
    collaborationService.inviteParticipantToCollaborateToThisInstance(project, user, projectCommand.participants)
    project
  }

  def addExistingActionToProject(idProject, idAction) {
    def project = Project.get(idProject)
    def action = Action.get(idAction)
    action.project=project
    action.save()
    project.addToActions(action).save()
    project
  }

  def addNotExistingActionToProject(userId, idProject, params){
    def user = User.get(userId)
    def project = Project.get(idProject)
    Action action = actionService.createAction(user.id, params)
    project.addToActions(action).save()
    project
  }

  def addExistingMeetingToProject(idProject, idMeeting) {
    def project = Project.get(idProject)
    def meeting = Meeting.get(idMeeting)
    project.addToMeetings(meeting).save()
    project
  }

  def addNotExistingMeetingToProject(userId, idProject, params) {
    def user = User.get(userId)
    def project = Project.get(idProject)
    Meeting meeting = meetingService.createMeeting(userId, params)
    project.addToMeetings(meeting).save()
    println project.errors
    project
  }

}
