package com.mymapmanager

import grails.transaction.Transactional
import com.mymapmanager.*

@Transactional
class MeetingService {

  def actionService
  def collaborationService

  // TODO : Refactor para diseñar a una sola clase todos las creaciones
  // de elementos que implementen Collaboration
  Meeting createMeeting(userId, meetingCommand){
    def user = User.get(userId)
    def appointmentDayFinished = new Date().parse("dd/MM/yyyy h:mm a", meetingCommand.appointmentDay)
    def dateHours = new Date().parse("h:mm a",meetingCommand.appointmentDayFinished)
    appointmentDayFinished.setHours(dateHours.hours)
    appointmentDayFinished.setMinutes(dateHours.minutes)
    def meeting = new Meeting (reason:meetingCommand.reason, 
                        	    venue:meetingCommand.venue, 
                        	    appointmentDay:new Date().parse("dd/MM/yyyy h:mm a", meetingCommand.appointmentDay), 
                        	    appointmentDayFinished:appointmentDayFinished,
                              autor:user)
    meeting.save()
    collaborationService.inviteParticipantToCollaborateToThisInstance(meeting, user, meetingCommand.participants)
    meeting
  }

  Meeting addNotExistingActionToExistingMeeting(userId, idMeeting, params){
    def meeting = Meeting.get(idMeeting)
    Action action = actionService.createAction(userId, params)
    meeting.addToActions(action).save()
    println meeting.errors
    meeting
  }

  Meeting addExistingActionToExistingMeeting(idMeeting, idAction){
    def meeting = Meeting.get(idMeeting)
    meeting.addToActions(Action.get(idAction)).save()
    meeting
  }

}