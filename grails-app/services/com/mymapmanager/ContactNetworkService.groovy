package com.mymapmanager

import grails.transaction.Transactional
import com.mymapmanager.*

@Transactional
class ContactNetworkService {

  def userService

  def addOneUserToContactNetworkOfCurrentUser(currentUserId, emailAnotherUser){
    User currentUser = User.get(currentUserId)
    User anotherUser
    if (User.countByUsername(emailAnotherUser))
      anotherUser = User.findByUsername(emailAnotherUser)
    else{
      anotherUser = userService.createUserWithUsernamePasswordAndEnable(emailAnotherUser,emailAnotherUser,false)
    }
    currentUser.addToContactNetwork(anotherUser)
    anotherUser.addToContactNetwork(currentUser)
  }
   
}