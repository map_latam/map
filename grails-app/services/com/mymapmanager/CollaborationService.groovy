package com.mymapmanager

import grails.transaction.Transactional
import com.mymapmanager.*

@Transactional
class CollaborationService {

  def contactNetworkService

  def inviteParticipantToCollaborateToThisInstance(def instance, def currentUser, def usersEmailString){
    if(!Collaboration.class.isAssignableFrom(instance.class))
      throw new Exception("Pelaz!")

    if(!usersEmailString) return ;

    def usersEmail = usersEmailString.tokenize(",")*.trim()

    def collaborationLink = new CollaborationLink(
      collaborationRef:instance.id,
      type:instance.class.getSimpleName())
      .save()
    def listEmails = []
    listEmails.addAll(usersEmail)
    listEmails.each{ email ->
      contactNetworkService.addOneUserToContactNetworkOfCurrentUser(currentUser.id,email)
      User user = User.findByUsername(email)
      collaborationLink.addToParticipants(new Collaborator(user:user,privilege:PrivilegeType.READ))
      collaborationLink.save()
    }
    collaborationLink
  }
 
}