modules = {

  model {
    resource url:'js/model/action.js'
    resource url:'js/model/actions.js'
    resource url:'js/model/project.js'
    resource url:'js/model/projectView.js' 
    resource url:'js/model/meeting.js'
    resource url:'js/model/meeting_controller.js'
  }

  dashboard {
    dependsOn 'model','handlebars'
    resource url:'js/home/app.js'
  }

  bootstrap{
    dependsOn 'jquery'
    resource url:'js/third-party/bootstrap/dist/css/bootstrap.min.css'
    resource url:'js/third-party/bootstrap/dist/js/bootstrap.min.js'
  }

  jqueryUi{
    resource url:'js/third-party/jquery-ui-amd/jquery-ui-1.10.0/ui/jquery-ui.js'
    resource url:'js/third-party/jquery-ui-amd/jquery-ui-1.10.0/themes/base/jquery-ui.css'
  }

  fontAwesome{
    resource url:'js/third-party/fontawesome/css/font-awesome.min.css'
  }

  theme{
    resource url:'css/theme.css'
  }

  ionicons {
    resource url:'lte/css/ionicons.min.css'
  }

  lte {
    dependsOn 'bootstrap', 'fontAwesome', 'ionicons'
    resource url:'lte/css/iCheck/minimal/minimal@2x.png'
    resource url:'lte/css/AdminLTE.css'
    resource url:'lte/js/AdminLTE/app.js'
  }

  application {
    resource url:'js/application.js'
  }

  project {
    resource url:'js/project/show.js'
  }

  contactNetwork {
    resource url:'js/contactNetwork/index.js'
  }

  moment{
    resource url:'js/third-party/moment/moment.js'
  }

  datePicker{
    dependsOn 'moment'
    resource url:'js/third-party/bootstrap3-datetimepicker/build/css/bootstrap-datetimepicker.min.css'
    resource url:'js/third-party/bootstrap3-datetimepicker/build/js/bootstrap-datetimepicker.min.js'
    resource url:'js/datePicker/datePickerConfig.js'
  }

  handlebars {
    resource url:'js/third-party/handlebars/handlebars.min.js'
  }

  wysihtml5 {
    resource url:'js/third-party/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.min.js'
    resource url:'js/third-party/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.css'
    resource url:'js/wysihtml5/wysihtml5-init.js'
    resource url:'css/wysihtml5/wysiwyg-color.css'
    resource url:'css/wysihtml5/customStyle.css'
  }

  login {
    resource url:'css/login/auth.css' 
  }

  home {
    resource url:'css/home/index.css' 
  }

  tagIt{
    dependsOn "jqueryUi"
    resource url:'js/third-party/tagit/js/tag-it.min.js'
    resource url:'js/third-party/tagit/css/jquery.tagit.css'
    resource url:'js/tag-it/tag-it-init.js'
  } 

  dropZone{
    resource url:'js/third-party/dropzone/downloads/dropzone.min.js'
    resource url:'css/dropZone/dropzone.css'
    resource url:'images/dropZone/spritemap.png'
    resource url:'js/dropZone/dropZone-init.js'
  }

  inputMask {
    resource url:'js/third-party/jquery.inputmask/dist/min/jquery.inputmask.js'
    resource url:'js/third-party/jquery.inputmask/dist/min/jquery.inputmask.date.extensions.js'
  }

  topic{
    resource url:'js/topic/modalTopic.js'
  }

  showDescription{
    resource url:'css/description/description.css'
  }

  privileges {
   resource url:'js/privileges/privilege.js'
  }

}