grails.servlet.version = "3.0"
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.work.dir = "target/work"
grails.project.target.level = 1.6
grails.project.source.level = 1.6

grails.project.fork = [
test: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, daemon:true],
run: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve:false],
war: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve:false],
console: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256]
]

grails.project.dependency.resolver = "maven"
grails.project.dependency.resolution = {
  inherits("global") {
  }
  log "error"
  checksums true
  legacyResolve false

  repositories {
    inherits true

    grailsPlugins()
    grailsHome()
    mavenLocal()
    grailsCentral()
    mavenCentral()
    mavenRepo "http://repo.spring.io/milestone/"
  }

  dependencies {
    runtime 'mysql:mysql-connector-java:5.1.30'
  }

  plugins {
    build ":tomcat:7.0.52.1"
    compile ":scaffolding:2.0.2"
    compile ':cache:1.1.1'
    runtime ":hibernate:3.6.10.9"
    runtime ":database-migration:1.4.0"
    runtime ":jquery:1.11.1"
    runtime ":resources:1.2.7"
    compile ":spring-security-core:2.0-RC2"
    compile ":amazon-s3:0.8.2"
    compile ':quartz:1.0.1'

  }
}
