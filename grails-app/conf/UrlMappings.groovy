class UrlMappings {

  static mappings = {
    "/$controller/$action?/$id?(.$format)?"{
      constraints {
      }
    }

    "/collaborator"(resource:'collaborator')

    "/"(view:"/index")
    "500"(view:'/error')
  }
}
