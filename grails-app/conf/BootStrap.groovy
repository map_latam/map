import com.mymapmanager.*

class BootStrap {

    def init = { servletContext ->
    createUsers()
    createRequestMapping()
    }
    def destroy = {
    }

    def createUsers(){
    if(!User.count()){
      def roleUser = new Role(authority:"ROLE_USER").save(flush:true)
      def roleAdmin = new Role(authority:"ROLE_ADMIN").save(flush:true)
      def user1 = new User(username:"user@map.com",password:"user",enabled:true).save(flush:true)
      def user2 = new User(username:"anotherUser@map.com",password:"anotherUser",enabled:true).save(flush:true)
      UserRole.create user1, roleUser, true
      UserRole.create user2, roleUser, true
      }
    }

    def createRequestMapping(){
    if(!Requestmap.count()){
      new Requestmap(url: '/js/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save()
      new Requestmap(url: '/css/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save()
      new Requestmap(url: '/assets/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save()
      new Requestmap(url: '/images/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save()
      new Requestmap(url: '/login/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save()
      new Requestmap(url: '/logout/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save()
      new Requestmap(url: '/', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save()
      new Requestmap(url: '/signUp/**', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save()
      new Requestmap(url: '/index', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save()
      new Requestmap(url: '/**/favicon.ico', configAttribute: 'IS_AUTHENTICATED_ANONYMOUSLY').save()
      new Requestmap(url: '/home/**', configAttribute: 'ROLE_USER').save()
      new Requestmap(url: '/project/**', configAttribute: 'ROLE_USER').save()
      new Requestmap(url: '/meeting/**', configAttribute: 'ROLE_USER').save()
      new Requestmap(url: '/action/**', configAttribute: 'ROLE_USER').save()
      new Requestmap(url: '/contactNetwork/**', configAttribute: 'ROLE_USER').save()
      new Requestmap(url: '/attachment/**', configAttribute: 'ROLE_USER').save()
      new Requestmap(url: '/topic/**', configAttribute: 'ROLE_USER').save()
    }
  }

}