<html>
<head>
  <title>. : My Map Manager - <g:layoutTitle default="Welcome"/> : .</title>
  <r:require modules="bootstrap, fontAwesome, theme"/>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
  <g:layoutHead/>
  <r:layoutResources />
</head>
<body>
  <g:render template="/common/header"/>
  <g:layoutBody/>
  <r:layoutResources />
</body>
</html>