<b>Attachments</b>
<g:if test="${attachmentLink}">
	<ul>
		<g:each var="attach" in="${attachmentLink?.attachments}">
			<li>
      ${attach.title}
        <div class="pull-right">
          <g:remoteLink action="deleteAttachToThisInstance" controller="attachment" id="${attach.id}"  params="[attachmentLink:attachmentLink.id]" update="update">
            <i class="fa fa-trash-o"></i></a>&nbsp;&nbsp;
          </g:remoteLink>
          <a href="${attach.url()}"><i class="fa fa-download"></i></a>
        </div>
			</li>
		</g:each>
	</ul>
</g:if>
<g:else>
	<div class="alert alert-info" style="margin-bottom: 0!important;">
    Still no attachments in this element!
  </div>
</g:else>

