<form id="upload" class="dropzone dz-clickable">
	<div class="dz-default dz-message">
		<span>Drop files here to upload</span>
		<g:hiddenField name="clazz" value="${instanceClazz}" />
		<g:hiddenField name="instanceId" value="${instanceId}" />
	</div>
</form>
