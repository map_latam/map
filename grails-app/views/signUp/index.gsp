<!DOCTYPE html>
<html>
<head>
  <meta name='layout' content='bootstrap'/>
  <title><g:message code="springSecurity.login.title"/></title>
  <r:require module="login"/>
</head>
<body>

  <div class="container">
    <g:if test='${flash.error}'>
      <div class="alert alert-danger">
        <div class='login_message'>${flash.error}</div>
      </div>
    </g:if>

    <g:form controller="signUp" action="register" class="form-signin form" name="userCommand">
      <h2 class="form-signin-heading">Please sign up!</h2>
      
      <g:set var="fieldWithError" value=""/>
      <g:hasErrors bean="${userCommand}" field="username">
        <g:set var="fieldWithError" value="has-error"/>
      </g:hasErrors>
      <div class='input-group ${fieldWithError}'>
        <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
        <g:textField name="username" value="${userCommand.username}" placeholder="name@domain.com" class="form-control" />
      </div>
      
      <g:set var="fieldWithError" value=""/>
      <g:hasErrors bean="${userCommand}" field="password">
        <g:set var="fieldWithError" value="has-error"/>
      </g:hasErrors>
      <div class='input-group ${fieldWithError}'>
        <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
        <g:passwordField name="password" value="${userCommand.password}" placeholder="Password" class="form-control" />
      </div>

      <g:set var="fieldWithError" value=""/>
      <g:hasErrors bean="${userCommand}" field="confirmPassword">
        <g:set var="fieldWithError" value="has-error"/>
      </g:hasErrors>
      <div class='input-group ${fieldWithError}'>
        <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
        <g:passwordField name="confirmPassword" value="${userCommand.confirmPassword}" placeholder="Confirm password" class="form-control" />
      </div>
      <hr>
      
      <input type='submit' id="submit" value='${message(code: "signUp.register",default:"Sign up now!")}' class="btn btn-lg btn-primary btn-block"/>
    </g:form>

  </div> <!-- /container -->

  <script type='text/javascript'>
  <!--
  (function() {
    $("div[role=navigation]").remove();
  })();
  // -->
</script>
</body>
</html>