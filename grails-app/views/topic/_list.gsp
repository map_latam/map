<ul>
  <g:each var="topic" in="${topics.sort()}">
    <li>
      <a class="topics" href="${g.createLink(controller:'topic', action:'show', id:topic.id)}">${topic.title}</a>
    </li>
  </g:each>
</ul>