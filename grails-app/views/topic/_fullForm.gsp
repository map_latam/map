<g:formRemote name="fullTopicForm" onSuccess="modalHide()" update="topics" url="[controller: 'topic', action: 'update']">
  <div class="modal-header">
    <div class="form-horizontal">
      <div class="form-group">
        <label for="title" class="col-sm-1 control-label">Titulo</label>
        <div class="col-sm-11">
          <g:textField type="text" class="form-control" name="title" id="title" value="${topic?.title}"> </g:textField>
        </div>
      </div>
    </div>
  </div>
  <g:hiddenField name="idTopic" value="${topic?.id}" />
  <div class="modal-body">
    <div class="form-group">
      <label for="description" class="control-label">Description</label>
      <g:textArea class="form-control" id="fullFormDescription" name="description" value="${topic?.description}" placeholder="Description"></g:textArea>
    </div>
    <div class="form-horizontal">
      <div class="form-group">
        <label for="duration" class="col-sm-2 control-label">Duration</label>
        <div class="col-sm-4">
          <g:field type="number" class="form-control" name="duration" id="duration" min="0" value="${topic?.duration}" />
        </div>
        <label for="arrange" class="col-sm-2 control-label">Arrange</label>
        <div class="col-sm-4">
          <g:field type="number" class="form-control" name="arrange" id="arrange" min="1" value="${topic?.arrange}" />
        </div>
      </div>
    </div>
    <div class="col-md-12 form-group">
      <label for="participants">Participants:</label>
      <input type='text' name="participants" class="form-control"/>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary">Save changes</button>
  </div>
</g:formRemote>