<div id="topicFormModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="topicFormModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Agenda</h3>
  </div>

  <div class="panel-body">
    <g:formRemote class="form-horizontal" id="formTopic" name="formTopic" onSuccess="cleanBasicForm()" update="topics" url="[controller: 'topic', action: 'addTopicToMeeting']">
      <div class="input-group">
        <input type="text" class="form-control input-sm" id="title" name="title" placeholder="Topic title" required />
        <g:hiddenField name="idMeeting" value="${meeting.id}" />
        <span class="input-group-btn">
          <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-plus-sign"></i></button>
        </span>
      </div>
    </g:formRemote>

    <div id="topics">
      <g:render template="/topic/list" model="[topics:meeting.topics]"/>
    </div>
  </div>
</div>