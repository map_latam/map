<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="lte"/>
  <title>Contact Network</title>
  <r:require module="contactNetwork"/>
</head>
<body>
  <div class="container">
    <h1><i class="fa fa-globe "></i> - Contact Network</h1>
    <hr>
    <div class="row">
      <div class="col-md-6">
        <h4>Add people to your Contact Network</h4>
        <g:formRemote name="addContact" update="updateMe" onSuccess="success()" method="GET" url="[controller: 'contactNetwork', action: 'findUser']" role="form">
          <div class="form-group">
            <input type="email" name="username" class="form-control" id="username" placeholder="Enter email">
          </div>
          <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
        </g:formRemote>
      </div>
      <div id="updateMe" class="col-md-6">
        <g:render template="yourContacts" model="[list:list]"/>
      </div>
    </div>
  </div>
</body>
</html>