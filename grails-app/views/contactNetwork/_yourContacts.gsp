<h4>Yours Contacts</h4> 
<table class="table table-hover">
  <thead>
    <th>#</th>
    <th>Email</th>  
    <th>User of MAP</th> 
  </thead>
  <tbody>
    <g:each in="${list}" var="user" status="i" >
      <tr>
        <td>${i+1}</td>
        <td>${user.username}</td>
        <td>
          <g:if test="${user.enabled}">
            <i class="fa fa-smile-o"></i>
          </g:if>
          <g:else>
            <i class="fa fa-frown-o"></i>
          </g:else>
        </td>
      </tr>  
    </g:each>
  </tbody>
</table>