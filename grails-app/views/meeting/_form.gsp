<div class="row">
  <div class="col-xs-12">
    <div class="form-group">
      <label for="reason">Reason of meeting:</label>
      <g:textField class="form-control" name="reason" style="overflow:auto; resize:none" required=""></g:textField>
      <label for="venue">Venue of meeting:</label>
      <g:textField class="form-control" name="venue" style="overflow:auto; resize:none" required=""></g:textField>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-${colxs ?: 4} form-group">
    <label for="appointmentDay">Appointment day:</label>
    <div class="input-group date">
      <div class="input-group-addon">
        <i class="fa fa-calendar"></i>
      </div>
      <g:textField name="appointmentDay" class="form-control calendar" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""/>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-${colxs ?: 4} form-group">
    <label for="finishedDay">Starting at:</label>
    <div class='input-group date'>
      <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
      <g:textField name="startingHour" class="form-control time" />
    </div>
    <label for="finishedDay">Finishing at:</label>
    <div class='input-group date'>
      <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
      <g:textField name="endingHour" class="form-control time" />
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-${colxs ?: 4} form-group">
    <label for="finishedDay">Recommended durations:</label>
    <div id="durations" class="input-group btn-group">
      <button type="button" class="btn btn-default" value="15">15 mins.</button>
      <button type="button" class="btn btn-default" value="30">30 mins.</button>
      <button type="button" class="btn btn-default" value="60">60 mins.</button>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12 form-group">
    <label for="participants">Participants:</label>
    <input type='text' name="participants" class="form-control"/>
  </div>
</div>
