<html>
<head>
	<meta name="layout" content="bootstrap"/>
	<title></title>
</head>
<body>
	<div class="container">
		<h2>Project: ${project.name}</h2>
		<div class="row hidden-sm hidden-xs">
			<div class="col-md-12">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
              <th>Reason</th>     
              <th>venue</th>     
              <th>Appointment Day</th>
              <th>Finished Hour</th>
						</tr>
					</thead>
					<tbody>
						<g:each var="meeting" in="${listMeetings}" status="i">
							<tr>
								<td>${i+1}</td>
                <td><g:link action="show" id="${meeting.id}">${meeting.reason}</g:link></td>
                <td>${meeting.venue}</td>
                <td>${meeting.appointmentDay.format("dd/MMMM/yyyy h:mm a")}</td>
                <td>${meeting.appointmentDayFinished.format("h:mm a")}</td>
							</tr>
						</g:each>
					</tbody>
				</table>
        <g:if test="${total>10}">
    		  <g:paginate controller="meeting" action="listMeetingsOfProject" total="${total}" params="[id:project.id]"/>
        </g:if>
			</div>
		</div>
		<div class="row visible-sm visible-xs">
      <div class="col-md-12">
        <div class="list-group">
          <g:each in="${listMeetings}" var="meeting" status="i" >
            <g:link class="list-group-item" action="show" id="${meeting.id}">${meeting.reason}</g:link>
          </g:each>
        </div>
        <g:if test="${total>10}">
          <g:paginate controller="meeting" action="listMeetingsOfProject" total="${total}" params="[id:project.id]"/>
        </g:if>
      </div>
    </div>
		<div class="col-md-6">
			<g:link class="btn btn-primary" controller="project" action="show" id="${project.id}">Return</g:link>
		</div>
	</div>
</body>
</html>
