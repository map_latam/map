<div class="row">
  <div class="col-md-4">
    <dl>
      <dt>Name</dt>
      <dd>${meeting.reason}</dd>
      <dt>Place</dt>
      <dd>${meeting.venue}</dd>
      <dt>Appointment Day</dt>
      <dd>${meeting.appointmentDay.format("dd/MMMM/yyyy h:mm a")}</dd>
      <dt>Finished Hour</dt>
      <dd>${meeting.appointmentDayFinished.format("h:mm a")}</dd>
    </dl>
    <b>Actions:</b><br>
    <div class="btn-group">
      <g:link class="btn btn-primary" controller="action" action="listActionsMeeting" params="[id:meeting.id]">See Actions</g:link>
      <span id="actions" class="btn btn-primary">${sizeOfActions}</span>
    </div>
  </div>
  <div class="col-md-4" id="panelParticipants">
    <g:render template="/collaborator/participants"
              model="[collaborationLink:collaborationLink,
                      instanceClass:'meeting']" />
  </div>
  <div class="col-md-4">
    <g:render template="/topic/basicForm" model="[meeting:meeting]"/>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div>
      <g:addAttachmentsForThisInstance instance="${meeting}" />
    </div>
  </div>
  <div class="col-md-6">
    <div id="update">
      <g:render template="/attachment/attachment" model="[instance:meeting]" />
    </div>
  </div>
</div>
