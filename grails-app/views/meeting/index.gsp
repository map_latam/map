<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="lte"/>
	<title>Create meeting</title>
  <r:require modules="jquery, datePicker, tagIt, jqueryUi, inputMask"/>
</head>
<body>
	<div class="container">
    <h1><i class="fa fa-pencil-square-o"></i> - New Meeting</h1>
    <div class="row">
    	<div class="col-md-12">
        <g:form class="form" name="meetingForm" action="save">
    		  <g:render template="form"/>
          <div class="form-group">
            <input class="btn btn-primary" type="submit" value="Create" />
          </div>
        </g:form>
    	</div>
    </div>
  </div>
</div>
</body>
</html>