<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="lte"/>
	<title>Show meeting</title>
  <r:require modules="project, datePicker, wysihtml5, topic, tagIt, dropZone, privileges"/>
</head>
<body>
	<div class="container">
    <h1><i class="fa fa-users"></i> - Meeting</h1>
    <div id="updateMe">
      <g:render template="meeting" model="[meeting:meeting,collaborationLink:collaborationLink]"/>
    </div>
    <hr>

    <div class="row">
      <div class="panel-group col-md-12" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                Create action
              </a>
            </h4>
          </div>
          <div id="collapseOne" class="panel-collapse collapse">
            <div class="panel-body">
              <div class="col-md-12">
                <g:formRemote class="form-horizontal" id="formAction" onSuccess="contador('action')" name="formAction" update="updateMe" url="[controller: 'meeting', action: 'addNotExistingActionToExistingMeeting']">
                  <div class="form-group">
                    <input class="form-control" name="name" type="text" placeholder="Name" required/>
                  </div>
                  <div class="form-group">
                    <g:textArea class="form-control" name="description" required placeholder="Description" css-url="${g.resource(dir:'css/wysihtml5', file:'wysiwyg-color.css')}"></g:textArea>
                  </div>
                  <div class="form-group">
                    <div class='input-group date' id='datetimepicker1' data-date-format="DD/MM/YYYY">
                      <input type='text' name="startingDay" class="form-control" placeholder="Starting day"/>
                      <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class='input-group date' id='datetimepicker2' data-date-format="DD/MM/YYYY">
                      <input type='text' name="finishedDay" class="form-control" placeholder="Finished day"/>
                      <span class="input-group-addon "><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <g:hiddenField name="idMeeting" value="${meeting.id}" />
                    <input class="btn btn-primary" type="submit" name="enviar" value="Create">
                  </div>
                </g:formRemote>
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                Add existing action
              </a>
            </h4>
          </div>
          <div id="collapseTwo" class="panel-collapse collapse">
            <div class="panel-body">
              <div id="updateActions">
                <form>
                  <g:hiddenField name="idMeeting" value="${meeting.id}"/>
                  <g:submitToRemote class="btn btn-primary" name="updateActions" update="updateActions" value="Action links" url="[controller: 'action', action: 'listActionsWithoutProjectOrMeeting']"/>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</body>
</html>