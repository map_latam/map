<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="lte"/>
  <title>Your meetings</title>
</head>
<body>
  <div class="container">
    <h1><i class="fa fa-list"></i> -  Meetings</h1>
    <div class="row hidden-sm hidden-xs">
      <div class="col-md-12">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>Reason</th>     
              <th>Venue</th>
              <th>Appointment day</th>     
              <th>Finished hour</th>
            </tr>
          </thead>
          <tbody>
            <g:each in="${meetings}" var="meeting" status="i" >
              <tr>
                <td>${i+1}</td>
                <td><g:link action="show" id="${meeting.id}">${meeting.reason}</g:link></td>
                <td>${meeting.venue}</td>
                <td>${meeting.appointmentDay.format("dd/MMMM/yyyy h:mm a")}</td>
                <td>${meeting.appointmentDayFinished.format("h:mm a")}</td>
              </tr>  
            </g:each>
          </tbody>
        </table>
        <g:if test="${total>10}">
          <g:paginate controller="meeting" action="list" total="${total}"/>
        </g:if>
      </div>
    </div>
    <div class="row visible-sm visible-xs">
      <div class="col-md-12">
        <div class="list-group">
          <g:each in="${meetings}" var="meeting" status="i" >
            <g:link class="list-group-item" action="show" id="${meeting.id}">${meeting.reason}</g:link>
          </g:each>
        </div>
        <g:if test="${total>10}">
          <g:paginate controller="meeting" action="list" total="${total}"/>
        </g:if>
      </div>
    </div>
  </div>
</body>
</html>