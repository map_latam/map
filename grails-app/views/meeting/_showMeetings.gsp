<g:each var="meeting" in="${meetings}">
  <p>
    <g:remoteLink update="updateMe" onSuccess="contador('meetings')" controller="project" action="addExistingMeetingToProject" params="[idMeeting:meeting.id,idProject:params.idProject]">${meeting.reason}</g:remoteLink>
  </p>
</g:each> 