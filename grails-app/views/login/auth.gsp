<html>
<head>
  <meta name='layout' content='bootstrap'/>
  <title><g:message code="springSecurity.login.title"/></title>
  <r:require module="login"/>
</head>

<body>

  <div class="container">
    <g:if test='${flash.message}'>
      <div class="alert alert-danger">
        <div class='login_message'>${flash.message}</div>
      </div>
    </g:if>

    <form action='${postUrl}' method='POST' id='loginForm' class="form-signin" role="form" autocomplete='off'>
      <h2 class="form-signin-heading">Please sign in</h2>
      <input type="email" class="form-control" name='j_username' id='username' placeholder="${message(code:'springSecurity.login.username.label')}" required autofocus>
      <input type="password" class="form-control" placeholder="${message(code:'springSecurity.login.password.label')}" name='j_password' id='password' required>
      <label class="checkbox">
        <input type='checkbox' value="remember-me" class='chk' name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if>/> Remember me
      </label>
      <input type='submit' id="submit" value='${message(code: "springSecurity.login.button")}' class="btn btn-lg btn-primary btn-block"/>
    </form>

  </div> <!-- /container -->

<script type='text/javascript'>
  <!--
  (function() {
    document.forms['loginForm'].elements['j_username'].focus();
    $("div[role=navigation]").remove();
  })();
  // -->
</script>
</body>
</html>