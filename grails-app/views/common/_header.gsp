<div class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <g:link uri="/home" class="navbar-brand">
        <g:img uri="https://www.mymapmanager.com/static/lq4gn08qA2gUrGTnIUBE9FObPO0sIlxiLQcPWIU8ZcR.png"/>
      </g:link>
    </div>
    <sec:ifLoggedIn>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li><g:link controller="project" action="list"><i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;Projects</g:link></li>
          <li><g:link controller="action" action="list"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Actions</g:link></li>
          <li><g:link controller="meeting" action="list"><i class="fa fa-users"></i>&nbsp;&nbsp;Meetings</g:link></li>
          <li><g:link controller="contactNetwork" action="index"><i class="fa fa-globe"></i>&nbsp;&nbsp;Contact Network</g:link></li>
          <li class="dropdown"><g:link class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="glyphicon glyphicon-plus white"></i> Create</g:link>
            <ul class="dropdown-menu">
              <li><g:link controller="project" action="index">Create Project</g:link></li>
              <li><g:link controller="action" action="index">Create Action</g:link></li>
              <li><g:link controller="meeting" action="index">Create Meeting</g:link></li>
            </ul>
          </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><g:link controller='logout'<i class="fa fa-sign-out"></i>&nbsp;&nbsp;Logout</g:link></li>
        </ul>
      </div>
    </sec:ifLoggedIn>
    <sec:ifNotLoggedIn>
      <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
          <li><g:link controller='login' action='auth'><i class="fa fa-sign-in"></i>&nbsp;&nbsp;Login</g:link></li>
        </ul>
      </div>
    </sec:ifNotLoggedIn>
  </div>
</div> 