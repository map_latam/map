<html>
<head>
	<meta name="layout" content="bootstrap"/>
	<title></title>
</head>
<body>
	<div class="container">
		<h2><i class="glyphicon glyphicon-folder-open"></i> - Actions of ${project?.name ?: meeting.reason}</h2>
		<div class="row hidden-sm hidden-xs">
			<div class="col-md-12">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
              <th>Action</th>     
              <th>Starting day</th>     
              <th>Finished day</th>
						</tr>
					</thead>
					<tbody>
						<g:each var="action" in="${listActions}" status="i">
							<tr>
								<td>${i+1}</td>
                <td><g:link action="show" id="${action.id}">${action.name}</g:link></td>
                <td>${action.startingDay.format("dd/MMMM/yyyy")}</td>
                <td>${action.finishedDay.format("dd/MMMM/yyyy")}</td>
							</tr>
						</g:each>
					</tbody>
				</table>
        <g:if test="${project&&total>10}">
          <g:paginate controller="action" action="listActionsProject" total="${total}" params="[id:project.id]"/>
        </g:if>
        <g:if test="${meeting&&total>10}">
          <g:paginate controller="action" action="listActionsMeeting" total="${total}" params="[id:meeting.id]"/>
        </g:if>
			</div>
		</div>
		<div class="row visible-sm visible-xs">
      <div class="col-md-12">
        <div class="list-group">
          <g:each in="${listActions}" var="action" status="i" >
            <g:link class="list-group-item" action="show" id="${action.id}">${action.name}</g:link>
          </g:each>
        </div>
        <g:if test="${project}">
          <g:paginate controller="action" action="listActionsProject" total="${total}" params="[id:project.id]"/>
        </g:if>
        <g:else>
          <g:paginate controller="action" action="listActionsMeeting" total="${total}" params="[id:meeting.id]"/>
        </g:else>
      </div>
    </div>
		<div class="col-md-6">
      <g:if test="${project}">
			 <g:link class="btn btn-primary" controller="project" action="show" id="${project.id}">Return</g:link>
      </g:if>
      <g:else>
        <g:link class="btn btn-primary" controller="meeting" action="show" id="${meeting.id}">Return</g:link>
      </g:else>
		</div>
	</div>
</body>
</html>
