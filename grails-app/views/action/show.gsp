<%@page defaultCodec="none" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="lte"/>
  <r:require modules="wysihtml5, dropZone, showDescription, privileges"/>
	<title>Show action</title>
</head>
<body>
	<div class="container">
    <h1><i class="fa fa-file-text-o"></i> - Action ${action.name}</h1>
    <div class="row">
    	<div class="col-md-4">
    		<dl>
          <dt>Description of action:</dt>
          <dd class="well" id="description">${action.description}</dd>
          <dt>Starting day:</dt>
          <dd>${action.startingDay.format("dd/MMMM/yyyy")}</dd>
          <dt>Finished day:</dt>
          <dd>${action.finishedDay.format("dd/MMMM/yyyy")}</dd>
        </dl>
      </div>
      <div class="col-md-4" id="panelParticipants">
        <g:render template="/collaborator/participants"
                  model="[collaborationLink:collaborationLink, instanceClass:'action']" />
      </div>
      <div class="col-md-4" id="update">
        <g:render template="/attachment/attachment" model="[instance:action]" />
      </div>
      <div class="col-md-12">
        <g:addAttachmentsForThisInstance instance="${action}" />
      </div>
    </div>
  </div>
</body>
</html> 