<ul id="my-actions" class="todo-list ui-sortable">  

</ul>

<script id="actions-template" type="text/x-handlebars-template">
  {{#each this}}
  <li>
    <!-- drag handle -->
    <span class="handle">
      <i class="fa fa-ellipsis-v"></i>
      <i class="fa fa-ellipsis-v"></i>
    </span>  
    <!-- checkbox -->
    <div class="icheckbox_minimal" aria-checked="false" aria-disabled="false" style="position: relative;">
      <input type="checkbox" value="" name="" style="position: absolute; opacity: 0;">
      <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background-color: rgb(255, 255, 255); border: 0px; opacity: 0; background-position: initial initial; background-repeat: initial initial;">
      </ins>
    </div>                                            
    <!-- todo text -->
    <span class="text">{{name}}</span>
    <!-- Emphasis label -->
    <small class="label label-danger"><i class="fa fa-clock-o"></i> 2 mins</small>
    <!-- General tools such as edit or delete-->
    <div class="tools">
      <i class="fa fa-edit"></i>
      <i class="fa fa-trash-o"></i>
    </div>
  </li>
  {{/each}}
</script>

<input type="hidden" id="action-post" value="${createLink(controller:'action',action:'save')}?format=json"/>
<input type="hidden" id="action-list" value="${createLink(controller:'action',action:'list')}?format=json"/>
