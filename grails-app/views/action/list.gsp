<%@page defaultCodec="none" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="lte"/>
  <title>Your Actions</title>
</head>
<body>
  <div class="container">
    <h1><i class="fa fa-list"></i> - Actions</h1>
    <div class="row hidden-sm hidden-xs">
      <div class="col-md-12">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>Action</th>     
              <th>Starting day</th>     
              <th>Finished day</th>
            </tr>
          </thead>
          <tbody>
            <g:each in="${actions}" var="action" status="i" >
              <tr>
                <td>${i+1}</td>
                <td><g:link action="show" id="${action.id}">${action.name}</g:link></td>
                <td>${action.startingDay.format("dd/MMMM/yyyy")}</td>
                <td>${action.finishedDay.format("dd/MMMM/yyyy")}</td>
              </tr>  
            </g:each>
          </tbody>
        </table>
        <g:if test="${total>10}">
          <g:paginate controller="action" action="list" total="${total}"/>
        </g:if>
      </div>
    </div>
    <div class="row visible-sm visible-xs">
      <div class="col-md-12">
        <div class="list-group">
          <g:each in="${actions}" var="action" status="i" >
            <g:link class="list-group-item" action="show" id="${action.id}">${action.name}</g:link>
          </g:each>
        </div>
        <g:if test="${total>10}">
          <g:paginate controller="action" action="list" total="${total}"/>
        </g:if>
      </div>
    </div>
  </div>
</body>
</html>