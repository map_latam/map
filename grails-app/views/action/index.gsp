<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="lte"/>
  <r:require modules="jquery, datePicker, wysihtml5, tagIt, jqueryUi"/>
	<title>Create Action</title>
</head>
<body>
	<div class="container">
    <h1><i class="fa fa-pencil-square-o"></i> - New Action</h1>
    <div class="row">
    	<div class="col-md-12">
    		<g:form class="form" name="actionForm" action="save">
        	<div class="col-md-12 form-group">
        		<label for="name">Name of action:</label>
        	  <g:textArea class="form-control" name="name" style="overflow:auto; resize:none" required=""></g:textArea>
        		<label for="description">Description of accion:</label>
            <g:textArea id="wysihtml5" class="form-control"  name="description" css-url="${g.resource(dir:'css/wysihtml5', file:'wysiwyg-color.css')}" required=""></g:textArea>
          </div>
          <div class="col-md-6 form-group">
            <label for="startingDay">Starting day:</label>
            <div class='input-group date' data-date-format="DD/MM/YYYY">
              <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
              <input type='text' id='datetimepicker1' name="startingDay" class="form-control" readonly/>
            </div>
          </div>
          <div class="col-md-6 form-group">
            <label for="finishedDay">Finished day:</label>
            <div class='input-group date'  data-date-format="DD/MM/YYYY">
              <span class="input-group-addon "><span class="glyphicon glyphicon-calendar"></span></span>
              <input type='text' id='datetimepicker2' name="finishedDay" class="form-control" readonly/>
            </div>
          </div>
          <div class="col-md-12 form-group">
            <label for="participants">Participants:</label>
            <input type='text' name="participants" class="form-control"/>
          </div>
        	<div class="form-group">
        	  <input class="btn btn-primary" type="submit" value="Create" />
        	</div>
      	</g:form>
    	</div>
    </div>
  </div>
</body>
</html>