<g:each var="action" in="${actions}">
  <p>
    <g:if test="${idProject}">
      <g:remoteLink update="updateMe" controller="project" action="addExistingActionToProject" params="[idAction:action.id,idProject:idProject]" onSuccess="contador('action')">${action.name}</g:remoteLink>
    </g:if>
    <g:else>
      <g:remoteLink update="updateMe" controller="meeting" action="addExistingActionToExistingMeeting" params="[idAction:action.id,idMeeting:params.idMeeting]" onSuccess="contador('action')">${action.name}</g:remoteLink>
    </g:else>
  </p>
</g:each>
<g:paginate controller="project" action="show" total="${total}" params="[id:idProject]"/>