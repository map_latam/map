<g:if test="${collaborationLink}">
  <div class="box">
    <div class="box-header">
      <h1 class="box-title"><i class="fa fa-fw fa-users"></i> Participants:</h1>
    </div>
    <div class="box-body">
      <ul class="todo-list ui-sortable">
        <g:each var="colaborator" in="${collaborationLink.participants}">
          <li>
            <span class="text">${colaborator.user.username}</span>
            
            <g:each var="privilege" in="${com.mymapmanager.PrivilegeType.values()}">
              <g:if test="${privilege == colaborator.privilege}">
                <small class="label label-info">
                  <i class='fa fa-fw ${message(code:"${colaborator.privilege.code}")}'></i>
                </small>
              </g:if>
            </g:each>
            
            <div class="tools" style="color: gray;">
              <i class="fa fa-fw fa-eye" privileges="READ" id="${colaborator.id}" collaborationLinkId="${collaborationLink.id}"></i>
              <!-- TODO
                En el JS renderear esta plantilla
              -->
              <i class="fa fa-fw fa-pencil" privileges="WRITE" id="${colaborator.id}" collaborationLinkId="${collaborationLink.id}"></i>
              <i class="fa fa-fw fa-key" privileges="EXECUTE" id="${colaborator.id}" collaborationLinkId="${collaborationLink.id}"></i>
            </div>
          </li>
        </g:each>
      </ul>
    </div>
  </div>
</g:if>
<g:else>
  <b>Participants:</b>
  <div class="alert alert-info participants">
      <b></b> Still no participants on this ${instanceClass}!
  </div>
</g:else>