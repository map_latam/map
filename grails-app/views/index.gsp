<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="bootstrap"/>
		<title>Welcome to Grails</title>
	</head>
	<body>
		<div class="container">
			<div class="jumbotron">
				<h1>My MAP Manager!</h1>
				<p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
				<p>
					<g:link controller="signUp" class="btn btn-primary btn-lg">
						Sign up now »
					</g:link>
				</p>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="panel panel-info">
						<div class="panel-heading">
						<h3 class="panel-title">Application status</h3>
						</div>
						<div class="panel-body">
							<ul>
								<li>App version: <g:meta name="app.version"/></li>
								<li>Grails version: <g:meta name="app.grails.version"/></li>
								<li>Groovy version: ${GroovySystem.getVersion()}</li>
								<li>JVM version: ${System.getProperty('java.version')}</li>
								<li>Reloading active: ${grails.util.Environment.reloadingAgentEnabled}</li>
								<li>Controllers: ${grailsApplication.controllerClasses.size()}</li>
								<li>Domains: ${grailsApplication.domainClasses.size()}</li>
								<li>Services: ${grailsApplication.serviceClasses.size()}</li>
								<li>Tag Libraries: ${grailsApplication.tagLibClasses.size()}</li>
							</ul>
						</div>
					</div>
					<hr>
					<div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Installed plugins</h3>
            </div>
            <div class="panel-body">
              <ul>
	              <g:each var="plugin" in="${applicationContext.getBean('pluginManager').allPlugins}">
	              	<li>${plugin.name} - ${plugin.version}</li>
	              </g:each>
	            </ul>
            </div>
          </div>
				</div>
				<div class="col-md-9">
					<div class="page-header">
		        <h1>Welcome to MAP</h1>
		      </div>
		      <p>
		      	Congratulations, you have successfully started your first Grails application! At the moment this is the default page, feel free to modify it to either redirect to a controller or display whatever content you may choose. Below is a list of controllers that are currently deployed in this application, click on each to execute its default action:
		      </p>
		      <div id="controller-list" role="navigation">
		      	<h2>Available Controllers:</h2>
		      	<ul>
		      		<g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">
		      		<li class="controller"><g:link controller="${c.logicalPropertyName}">${c.fullName}</g:link></li>
		      	</g:each>
		      </ul>
		    </div>
				</div>
			</div>
		</div>

	</body>
</html>
