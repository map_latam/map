<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="lte"/>
  <title>Welcome to My MAP</title>
  <r:require modules="tagIt, inputMask, moment"/>
  <r:require module="dashboard"/>
</head>
<body>
  <section class="content-header">
    <h1>
      Dashboard
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3>
              ${meetingCount}
            </h3>
            <p>
              Meetings
            </p>
          </div>
          <div class="icon">
            <i class="fa fa-file-text-o"></i>
          </div>
          <a href="#" class="small-box-footer">
            See my meetings <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div><!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-blue">
          <div class="inner">
            <h3>
              <span class="actionCount">${actionCount}</span>
            </h3>
            <p>
              Actions
            </p>
          </div>
          <div class="icon">
            <i class="fa fa-comment-o"></i>
          </div>
          <a href="#" class="small-box-footer">
            Actions dashboard <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div><!-- ./col -->
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
          <div class="inner">
            <h3>
              <span class="projectCount">
                ${projectCount}
              </span>
            </h3>
            <p>
              Projects
            </p>
          </div>
          <div class="icon">
            <i class="fa fa-folder-open"></i>
          </div>
          <a href="#" class="small-box-footer">
            Current projects <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div><!-- ./col -->
      
      <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>
              0
            </h3>
            <p>
              Comments
            </p>
          </div>
          <div class="icon">
            <i class="fa fa-users"></i>
          </div>
          <a href="#" class="small-box-footer">
            Conversations <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div><!-- ./col -->
      
    </div>

    <hr>

    <div class="row">
      <div class="col-md-4">
        <g:form name="meetingForm">
        <!-- general form elements -->
        <div class="box box-warning">
          <div class="box-header">
            <i class="fa fa-calendar"></i>
            <h3 class="box-title">New meeting...</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <g:render template="/meeting/form" model="[colxs:12]"/>
          </div>
          <div class="box-footer clearfix no-border">
            <div class="row no-print">
              <div class="col-xs-12">
                <button class="btn btn-success pull-right" >
                  <i class="fa fa-plus"></i> Create project
                </button>
                <button class="btn btn-primary pull-right" style="margin-right: 5px;">
                  <i class="fa fa-edit"></i> Edit project
                </button>  
              </div>
            </div>
          </g:form>
        </div>
      </div><!-- /.box -->

      </div>
      <div class="col-md-4">
        <div class="box box-primary">
          <div class="box-header" style="cursor: move;">
            <i class="fa fa-check-square-o"></i>
            <h3 class="box-title">Actions</h3>
            <!--div class="box-tools pull-right">
              <ul class="pagination pagination-sm inline">
                <li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">»</a></li>
              </ul>
            </div-->
          </div><!-- /.box-header -->
          <div class="box-body">
            <g:render template="/action/quickForm"/>
            <g:render template="/action/quickList"/>
          </div><!-- /.box-body -->
          <div class="box-footer clearfix no-border">
            <g:link controller="action" action="list" class="btn btn-default pull-right">
              <i class="fa fa-eye"></i> See more actions...
            </g:link>
          </div>
        </div>

      </div>
      <div class="col-md-4" id="new-project">
        <!-- general form elements -->
        <div class="box box-success">
          <div class="box-header">
            <i class="fa fa-rocket"></i>
            <h3 class="box-title">New project...</h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">
            <div class='alert alert-success'>
              <i class='fa fa-check'></i>
              <span class="alert-body"></span>
            </div>
            <g:render template="/project/form"/>
            <input type="hidden" id="project-edit" value="${createLink(controller:'project',action:'index')}?"/>
            <input type="hidden" id="project-post" value="${createLink(controller:'project',action:'save')}?format=json"/>
            <input type="hidden" id="project-show" value="${createLink(controller:'project',action:'show', absolute:true)}/"/>
          </div>
          <div class="box-footer clearfix no-border">
            <div class="row no-print">
              <div class="col-xs-12">
                <button id="create" class="btn btn-success pull-right" >
                  <i class="fa fa-plus"></i> Create project
                </button>
                <button id="edit" class="btn btn-primary pull-right" style="margin-right: 5px;">
                  <i class="fa fa-edit"></i> Edit project
                </button>  
              </div>
            </div>
          </div>
        </div><!-- /.box -->

      </div>
    </div>

  </section>


</body>
</html>
