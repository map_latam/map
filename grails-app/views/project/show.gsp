<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="lte"/>
  <r:require modules="project, datePicker, wysihtml5, showDescription, dropZone, privileges"/>
	<title>Show Project</title>
</head>
<body>
	<div class="container"> 
    <h1><i class="glyphicon glyphicon-folder-open"></i> - ${project.name}</h1>
    <div id="updateMe">
      <g:render template="project" model="[project:project,collaborationLink:collaborationLink]"/>
    </div>

    <hr>

    <div class="row">
      <div class="panel-group col-md-6" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                Create action
              </a>
            </h4>
          </div>
          <div id="collapseOne" class="panel-collapse collapse">
            <div class="panel-body">
              <div class="col-md-12">
                <g:formRemote class="form-horizontal" id="formAction" onSuccess="contador('action')" name="formAction" update="updateMe" url="[controller: 'project', action: 'addNotExistingActionToProject']">
                  <div class="form-group">
                    <input class="form-control" name="name" type="text" placeholder="Name" required/>
                  </div>
                  <div class="form-group">
                    <g:textArea class="form-control" css-url="${g.resource(dir:'css/wysihtml5', file:'wysiwyg-color.css')}" name="description" required placeholder="Description"></g:textArea>
                  </div>
                  <div class="form-group">
                    <div class='input-group date' id='datetimepicker1' data-date-format="DD/MM/YYYY">
                      <input type='text' name="startingDay" class="form-control" placeholder="Starting day"/>
                      <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class='input-group date' id='datetimepicker2' data-date-format="DD/MM/YYYY">
                      <input type='text' name="finishedDay" class="form-control" placeholder="Finished day"/>
                      <span class="input-group-addon "><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <g:hiddenField name="idProject" value="${project.id}" />
                    <input class="btn btn-primary" type="submit" name="enviar" value="Create">
                  </div>
                </g:formRemote>
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                Add existing action
              </a>
            </h4>
          </div>
          <g:if test="${params.max}">
            <div id="collapseTwo" class="panel-collapse collapse in">
              <div class="panel-body">
                <div id="updateActions">
                  <g:render template="../action/showActions" model="[actions:actions,idProject:project.id,total:total]"/>
                </div>
              </div>
            </div>
          </g:if>
          <g:else>
            <div id="collapseTwo" class="panel-collapse collapse">
              <div class="panel-body">
                <div id="updateActions">
                  <g:render template="../action/showActions" model="[actions:actions,idProject:project.id,total:total]"/>
                </div>
              </div>
            </div>
          </g:else>
        </div>
      </div>

      <div class="panel-group col-md-6" id="accordion1">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion1" href="#collapseThree">
                Create meeting
              </a>
            </h4>
          </div>
          <div id="collapseThree" class="panel-collapse collapse">
            <div class="panel-body">
              <div class="col-md-12">
                <g:formRemote class="form-horizontal" onSuccess="contador('meetings')" name="formMeeting" update="updateMe" url="[controller: 'project', action: 'addNotExistingMeetingToProject']">
                  <div class="form-group">
                    <input class="form-control" name="reason" type="text" placeholder="Name" required/>
                  </div>
                  <div class="form-group">
                    <input class="form-control" name="venue" type="text" placeholder="Place" required/>
                  </div>
                  <div class="form-group">
                    <div class='input-group date' id='datetimepicker3'>
                      <input type='text' name="appointmentDay" class="form-control" placeholder="Appointment Day"/>
                      <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class='input-group date' id='datetimepicker4'>
                      <input type='text' name="appointmentDayFinished" class="form-control" placeholder="Finished"/>
                      <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                    </div>
                  </div>
                  <g:hiddenField name="idProject" value="${project.id}" />
                  <input class="btn btn-primary" type="submit" name="enviar" value="Create">
                </g:formRemote>
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion1" href="#collapseFour">
                Add existing meeting
              </a>
            </h4>
          </div>
          <div id="collapseFour" class="panel-collapse collapse">
            <div class="panel-body">
              <div id="updateMeetings">
                <form>
                  <g:hiddenField name="idProject" value="${project.id}" />
                  <g:submitToRemote class="btn btn-primary" name="updateMeetings" update="updateMeetings" value="Meeting links" url="[controller: 'meeting', action: 'listMeetingsWithoutProject']"/>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</body>
</html>