<%@page defaultCodec="none" %>
<!DOCTYPE html>
<html>
<head>
  <meta name="layout" content="lte"/>
  <title>Your projects</title>
  <r:require modules="wysihtml5"/>
</head>
<body>
  <div class="container">
    <h1><i class="fa fa-list"></i> - Projects</h1>
    <div class="row hidden-sm hidden-xs">
      <div class="col-md-12">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>Project</th>     
              <th>Starting day</th>     
              <th>Finished day</th>
            </tr>
          </thead>
          <tbody>
            <g:each in="${projects}" var="project" status="i" >
              <tr>
                <td>${i+1}</td>
                <td><g:link action="show" id="${project.id}">${project.name}</g:link></td>
                <td>${project?.startingDay.format("dd/MMMM/yyyy")}</td>
                <td>${project?.finishedDay.format("dd/MMMM/yyyy")}</td>
              </tr>  
            </g:each>
          </tbody>
        </table>
        <g:if test="${total>10}">
          <g:paginate controller="project" action="list" total="${total}"/>
        </g:if>
      </div>
    </div>
    <div class="row visible-sm visible-xs">
      <div class="col-md-12">
        <div class="list-group">
          <g:each in="${projects}" var="project" status="i" >
            <g:link class="list-group-item" action="show" id="${project.id}">${project.name}</g:link>
          </g:each>
        </div>
        <g:if test="${total>10}">
          <g:paginate controller="project" action="list" total="${total}"/>
        </g:if>
      </div>
    </div>
  </div>
</body>
</html>