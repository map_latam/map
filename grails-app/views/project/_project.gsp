<%@page defaultCodec="none" %>
<div class="row"> 
  <dl>
    <div class="col-md-6">
      <dd>
        <pre class="well" id="description">
          ${project.description}
        </pre>
      </dd>
    </div>
    <div class="col-md-6">
      <dt>Starting day:</dt>
      <dd>${project?.startingDay.format("dd/MMMM/yyyy")}</dd>
      <dt>Finished day:</dt>
      <dd>${project?.finishedDay.format("dd/MMMM/yyyy")}</dd>
      <hr>
      <div id="panelParticipants">
        <g:render template="/collaborator/participants"
                    model="[collaborationLink:collaborationLink, instanceClass:'project']" />
      </div>
    </div>
  </dl>  
</div>

<div class="row">
  <div class="col-md-6">
    <h5>Actions of project:</h5>
    <div class="btn-group">
      <g:link class="btn btn-primary" controller="action" action="listActionsProject" params="[id:project.id]">See Actions</g:link>
      <span id="actions" class="btn btn-primary">${sizeOfActions}</span>
    </div>
  </div>
  <div class="col-md-6">
    <h5>Meetings of project:</h5>
    <div class="btn-group">
      <g:link class="btn btn-primary" controller="meeting" action="listMeetingsOfProject" params="[id:project.id]">See Meetings</g:link>
        <span id="meetings" class="btn btn-primary">${sizeOfMeetings}</span>
    </div>  
  </div>
</div>
<hr>
<div class="row">
  <div class="col-md-6">
    <g:addAttachmentsForThisInstance instance="${project}" />
  </div>  
  <div class="col-md-6" id="update">
    <g:render template="/attachment/attachment" model="[instance:project]" />
  </div>
</div>
