  <div class="col-md-12 form-group">
    <label for="name">Name of project:</label>
    <input type='text' name="name" class="form-control" required="" value="${projectCommand?.name}"/>
    <label for="description">Description of project:</label>
    <textArea class="form-control" rows="5" id="wysihtml5" name="description" css-url="${g.resource(dir:'css/wysihtml5', file:'wysiwyg-color.css')}" required maxlength="1000">${projectCommand?.description}</textArea>
    <span class="help-block" id="left-counter"><p><span id="counter">0</span> / 200</p></span>
  </div>
  <div class="col-md-6 form-group">
    <label for="startingDay">Starting day:</label>
    <div class='input-group date' data-date-format="DD/MM/YYYY">
      <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
      <g:textField name="startingDay" class="form-control calendar" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" value="${projectCommand?.startingDay}"/>
    </div>
  </div>
  <div class="col-md-6 form-group">
    <label for="finishedDay">Finished day:</label>
    <div class='input-group date'  data-date-format="DD/MM/YYYY">
      <span class="input-group-addon "><span class="glyphicon glyphicon-calendar"></span></span>
      <g:textField name="finishedDay" class="form-control calendar" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" value="${projectCommand?.finishedDay}"/>
    </div>
  </div>
  
  <div id="participants" class="col-md-12 form-group">
    <label id="participants" class="control-label" for="participants">Participants:</label>
    <div id="message" class="alert alert-danger" style="display:none;">Invalid email</div>
    <input type="text" name="participants" class="form-control" id="inputError2" value="${projectCommand?.participants}">
  </div>
