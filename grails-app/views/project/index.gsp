<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="lte"/>
  <r:require modules="jquery, datePicker, wysihtml5, tagIt"/>
	<title>Create Project</title>

</head>
<body>
	<div class="container">
    <h1><i class="fa fa-pencil-square-o"></i> - New Project</h1>
    <div class="row">
    	<div class="col-md-12">
        <g:form class="form" name="actionForm" action="save">
          <g:render template="form"/>
          <div class="form-group">
            <input class="btn btn-primary" type="submit" value="Create" />
          </div>
        </g:form>
    	</div>
    </div>
  </div>
</body>
</html>
