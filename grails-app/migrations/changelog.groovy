databaseChangeLog = {

	changeSet(author: "neodevelop (generated)", id: "1399993287423-1") {
    sql("set storage_engine=InnoDB;")
		createTable(tableName: "action") {
			column(autoIncrement: "true", name: "id", type: "BIGINT") {
				constraints(nullable: "false", primaryKey: "true")
			}

			column(name: "version", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "autor_id", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "DATETIME") {
				constraints(nullable: "false")
			}

			column(name: "description", type: "VARCHAR(1000)") {
				constraints(nullable: "false")
			}

			column(name: "finished_day", type: "DATETIME")

			column(name: "last_updated", type: "DATETIME") {
				constraints(nullable: "false")
			}

			column(name: "meeting_id", type: "BIGINT")

			column(name: "name", type: "VARCHAR(200)") {
				constraints(nullable: "false")
			}

			column(name: "project_id", type: "BIGINT")

			column(name: "starting_day", type: "DATETIME")
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-2") {
    sql("set storage_engine=InnoDB;")
		createTable(tableName: "attachment_link") {
			column(autoIncrement: "true", name: "id", type: "BIGINT") {
				constraints(nullable: "false", primaryKey: "true")
			}

			column(name: "version", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "attachment_ref", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "type", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-3") {
    sql("set storage_engine=InnoDB;")
		createTable(tableName: "attachment_link_s3asset") {
			column(name: "attachment_link_attachments_id", type: "BIGINT")

			column(name: "s3asset_id", type: "BIGINT")
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-4") {
    sql("set storage_engine=InnoDB;")
		createTable(tableName: "collaboration_link") {
			column(autoIncrement: "true", name: "id", type: "BIGINT") {
				constraints(nullable: "false", primaryKey: "true")
			}

			column(name: "version", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "collaboration_ref", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "type", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-5") {
    sql("set storage_engine=InnoDB;")
		createTable(tableName: "collaboration_link_collaborator") {
			column(name: "collaboration_link_participants_id", type: "BIGINT")

			column(name: "collaborator_id", type: "BIGINT")
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-6") {
    sql("set storage_engine=InnoDB;")
		createTable(tableName: "collaborator") {
			column(autoIncrement: "true", name: "id", type: "BIGINT") {
				constraints(nullable: "false", primaryKey: "true")
			}

			column(name: "version", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "DATETIME") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "DATETIME") {
				constraints(nullable: "false")
			}

			column(name: "privilege", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}

			column(name: "user_id", type: "BIGINT") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-7") {
    sql("set storage_engine=InnoDB;")
		createTable(tableName: "meeting") {
			column(autoIncrement: "true", name: "id", type: "BIGINT") {
				constraints(nullable: "false", primaryKey: "true")
			}

			column(name: "version", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "appointment_day", type: "DATETIME") {
				constraints(nullable: "false")
			}

			column(name: "appointment_day_finished", type: "DATETIME")

			column(name: "autor_id", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "DATETIME") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "DATETIME") {
				constraints(nullable: "false")
			}

			column(name: "project_id", type: "BIGINT")

			column(name: "reason", type: "VARCHAR(200)") {
				constraints(nullable: "false")
			}

			column(name: "venue", type: "VARCHAR(1000)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-8") {
    sql("set storage_engine=InnoDB;")
		createTable(tableName: "project") {
			column(autoIncrement: "true", name: "id", type: "BIGINT") {
				constraints(nullable: "false", primaryKey: "true")
			}

			column(name: "version", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "autor_id", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "DATETIME") {
				constraints(nullable: "false")
			}

			column(name: "description", type: "VARCHAR(1000)") {
				constraints(nullable: "false")
			}

			column(name: "finished_day", type: "DATETIME")

			column(name: "last_updated", type: "DATETIME") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "VARCHAR(200)") {
				constraints(nullable: "false")
			}

			column(name: "starting_day", type: "DATETIME") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-9") {
    sql("set storage_engine=InnoDB;")
		createTable(tableName: "requestmap") {
			column(autoIncrement: "true", name: "id", type: "BIGINT") {
				constraints(nullable: "false", primaryKey: "true")
			}

			column(name: "version", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "config_attribute", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}

			column(name: "url", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-10") {
    sql("set storage_engine=InnoDB;")
		createTable(tableName: "role") {
			column(autoIncrement: "true", name: "id", type: "BIGINT") {
				constraints(nullable: "false", primaryKey: "true")
			}

			column(name: "version", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "authority", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-11") {
    sql("set storage_engine=InnoDB;")
		createTable(tableName: "s3asset") {
			column(autoIncrement: "true", name: "id", type: "BIGINT") {
				constraints(nullable: "false", primaryKey: "true")
			}

			column(name: "bucket", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}

			column(name: "bytes_per_second", type: "BIGINT")

			column(name: "bytes_transfered", type: "BIGINT")

			column(name: "description", type: "VARCHAR(255)")

			column(name: "host_name", type: "VARCHAR(255)")

			column(name: "aws_key", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}

			column(name: "last_modified", type: "BIGINT")

			column(name: "length", type: "BIGINT")

			column(name: "local_path", type: "VARCHAR(255)")

			column(name: "local_url", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}

			column(name: "mime_type", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}

			column(name: "percent_transferred", type: "DOUBLE")

			column(name: "protocol", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}

			column(name: "aws_status", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}

			column(name: "title", type: "VARCHAR(255)")
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-12") {
    sql("set storage_engine=InnoDB;")
		createTable(tableName: "s3asset_options") {
			column(name: "options", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "options_idx", type: "VARCHAR(50)") {
				constraints(nullable: "false")
			}

			column(name: "version", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "options_elt", type: "VARCHAR(200)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-13") {
    sql("set storage_engine=InnoDB;")
		createTable(tableName: "topic") {
			column(autoIncrement: "true", name: "id", type: "BIGINT") {
				constraints(nullable: "false", primaryKey: "true")
			}

			column(name: "version", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "arrange", type: "INT") {
				constraints(nullable: "false")
			}

			column(name: "date_created", type: "DATETIME") {
				constraints(nullable: "false")
			}

			column(name: "description", type: "VARCHAR(1000)")

			column(name: "duration", type: "INT") {
				constraints(nullable: "false")
			}

			column(name: "last_updated", type: "DATETIME") {
				constraints(nullable: "false")
			}

			column(name: "meeting_id", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "title", type: "VARCHAR(200)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-14") {
    sql("set storage_engine=InnoDB;")
		createTable(tableName: "user") {
			column(autoIncrement: "true", name: "id", type: "BIGINT") {
				constraints(nullable: "false", primaryKey: "true")
			}

			column(name: "version", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "account_expired", type: "BIT") {
				constraints(nullable: "false")
			}

			column(name: "account_locked", type: "BIT") {
				constraints(nullable: "false")
			}

			column(name: "enabled", type: "BIT") {
				constraints(nullable: "false")
			}

			column(name: "password", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}

			column(name: "password_expired", type: "BIT") {
				constraints(nullable: "false")
			}

			column(name: "username", type: "VARCHAR(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-15") {
    sql("set storage_engine=InnoDB;")
		createTable(tableName: "user_role") {
			column(name: "role_id", type: "BIGINT") {
				constraints(nullable: "false")
			}

			column(name: "user_id", type: "BIGINT") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-16") {
    sql("set storage_engine=InnoDB;")
		createTable(tableName: "user_user") {
			column(name: "user_contact_network_id", type: "BIGINT")

			column(name: "user_id", type: "BIGINT")
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-17") {
		addPrimaryKey(columnNames: "options, options_idx", tableName: "s3asset_options")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-18") {
		addPrimaryKey(columnNames: "role_id, user_id", tableName: "user_role")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-36") {
		createIndex(indexName: "url", tableName: "requestmap", unique: "true") {
			column(name: "url")
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-37") {
		createIndex(indexName: "authority", tableName: "role", unique: "true") {
			column(name: "authority")
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-38") {
		createIndex(indexName: "aws_key", tableName: "s3asset", unique: "true") {
			column(name: "aws_key")
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-39") {
		createIndex(indexName: "idx_asset_status", tableName: "s3asset", unique: "false") {
			column(name: "aws_status")
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-40") {
		createIndex(indexName: "option_name_idx", tableName: "s3asset_options", unique: "false") {
			column(name: "options_idx")
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-41") {
		createIndex(indexName: "username", tableName: "user", unique: "true") {
			column(name: "username")
		}
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-19") {
		addForeignKeyConstraint(baseColumnNames: "autor_id", baseTableName: "action", constraintName: "FKAB2F7E36F56104C6", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-20") {
		addForeignKeyConstraint(baseColumnNames: "meeting_id", baseTableName: "action", constraintName: "FKAB2F7E36F57BF696", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "meeting", referencesUniqueColumn: "false")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-21") {
		addForeignKeyConstraint(baseColumnNames: "project_id", baseTableName: "action", constraintName: "FKAB2F7E365A0E1D6", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "project", referencesUniqueColumn: "false")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-22") {
		addForeignKeyConstraint(baseColumnNames: "attachment_link_attachments_id", baseTableName: "attachment_link_s3asset", constraintName: "FKAAEBAF27B9483F54", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "attachment_link", referencesUniqueColumn: "false")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-23") {
		addForeignKeyConstraint(baseColumnNames: "s3asset_id", baseTableName: "attachment_link_s3asset", constraintName: "FKAAEBAF27BDA9C076", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "s3asset", referencesUniqueColumn: "false")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-24") {
		addForeignKeyConstraint(baseColumnNames: "collaboration_link_participants_id", baseTableName: "collaboration_link_collaborator", constraintName: "FKF21AACB18B26812A", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "collaboration_link", referencesUniqueColumn: "false")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-25") {
		addForeignKeyConstraint(baseColumnNames: "collaborator_id", baseTableName: "collaboration_link_collaborator", constraintName: "FKF21AACB141788EBE", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "collaborator", referencesUniqueColumn: "false")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-26") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "collaborator", constraintName: "FK8509F00696CD409E", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-27") {
		addForeignKeyConstraint(baseColumnNames: "autor_id", baseTableName: "meeting", constraintName: "FK38264A3BF56104C6", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-28") {
		addForeignKeyConstraint(baseColumnNames: "project_id", baseTableName: "meeting", constraintName: "FK38264A3B5A0E1D6", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "project", referencesUniqueColumn: "false")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-29") {
		addForeignKeyConstraint(baseColumnNames: "autor_id", baseTableName: "project", constraintName: "FKED904B19F56104C6", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-30") {
		addForeignKeyConstraint(baseColumnNames: "options", baseTableName: "s3asset_options", constraintName: "FKE6010E2F42CEF82A", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "s3asset", referencesUniqueColumn: "false")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-31") {
		addForeignKeyConstraint(baseColumnNames: "meeting_id", baseTableName: "topic", constraintName: "FK696CD2FF57BF696", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "meeting", referencesUniqueColumn: "false")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-32") {
		addForeignKeyConstraint(baseColumnNames: "role_id", baseTableName: "user_role", constraintName: "FK143BF46AF1A27CBE", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "role", referencesUniqueColumn: "false")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-33") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_role", constraintName: "FK143BF46A96CD409E", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-34") {
		addForeignKeyConstraint(baseColumnNames: "user_contact_network_id", baseTableName: "user_user", constraintName: "FK143D5FBF7BB0E9CE", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
	}

	changeSet(author: "neodevelop (generated)", id: "1399993287423-35") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "user_user", constraintName: "FK143D5FBF96CD409E", deferrable: "false", initiallyDeferred: "false", onDelete: "NO ACTION", onUpdate: "NO ACTION", referencedColumnNames: "id", referencedTableName: "user", referencesUniqueColumn: "false")
	}
}
