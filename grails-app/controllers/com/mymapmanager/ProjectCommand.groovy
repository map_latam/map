package com.mymapmanager

@grails.validation.Validateable
class ProjectCommand {

  String name 
  String description

  String startingDay
  String finishedDay

  String participants

  static constraints = {
    name blank:false, size:1..200
    description nullable:true, blank:true, size:1..1000
    startingDay nullable:true
    finishedDay nullable:true
  }

}
