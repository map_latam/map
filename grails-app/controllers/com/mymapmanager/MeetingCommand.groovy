package com.mymapmanager

@grails.validation.Validateable
class MeetingCommand {

  String reason
  String venue

  String appointmentDay
  String appointmentDayFinished

  Long idProject

  String participants

  static constraints = {
    reason blank:false, size:1..200
    venue blank:false, size:1..1000 
    appointmentDay blank:false
    appointmentDayFinished nullable:true
  }

}