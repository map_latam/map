package com.mymapmanager

class TopicController {

  def topicService
  def springSecurityService

  def addTopicToMeeting(TopicCommand topicCommand) {
    if(topicCommand.hasErrors()) {
      render(status: 400, text: "Request has errors")
    }

    def topic = topicService.create(topicCommand)
    render template:'list', model:[topics:topic.meeting.topics]
  }

  def show(Long id) {
    def topic = Topic.get(id)
    render template:'fullForm', model:[topic:topic]
  }

  def update(TopicUpdateCommand topicUpdateCommand){
    if(topicUpdateCommand.hasErrors()) {
      render(status: 400, text: "Request has errors")
    }

    def currentUser = springSecurityService.currentUser
    def topic = topicService.update(currentUser.username,topicUpdateCommand)
    render template:'list', model:[topics:topic.meeting.topics]
  }
}

class TopicUpdateCommand{
  Long idTopic
  String title
  String description
  Integer duration
  Integer arrange
  String participants

  static constraints = {
    idTopic min:1L
    title blank:false, size:1..200
    description nullable:true, blank:true, size:1..1000
    duration min:0
    arrange min:1
    participants nullable:true, blank:true
  }
}

class TopicCommand {

  String title
  Long idMeeting

  static constraints = {
    idMeeting min:1L
    title blank:false, size:1..200
  }

}
