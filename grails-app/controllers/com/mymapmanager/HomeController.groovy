package com.mymapmanager

class HomeController {

  def springSecurityService

  def index() {
    def user = springSecurityService.currentUser
    [
      projectCount:Project.countByAutor(user),
      actionCount:Action.countByAutor(user),
      meetingCount:Meeting.countByAutor(user)
    ]
  }

 def blank() { }

}