package com.mymapmanager

import static org.springframework.http.HttpStatus.*

class ActionController {

  def actionService
  def springSecurityService

  def index() { }

  def save(ActionCommand actionCommand){
    def currentUser = springSecurityService.currentUser
    Action action = actionService.createAction(currentUser.id, actionCommand)
    withFormat {
      form multipartForm {
        redirect action:"show", id:action.id
      }
      json { respond action, [status: CREATED] }
    }
	}

	def show(){
    def action = Action.get(params.id)
    def collaborationLink = CollaborationLink.findByCollaborationRefAndType(action.id,action.class.getSimpleName())
    [action:action,
    collaborationLink:collaborationLink]
  }

  def list(){
    def currentUser = springSecurityService.currentUser
    def actions = Action.findAllByAutor(currentUser, [max:params.max ?: 10, offset:params.offset ?: 0, sort: "dateCreated", order: "desc"])
    withFormat {
      html { [actions:actions, total:Action.countByAutor(currentUser)] }
      json { respond actions, [status: OK] }
    }
  }

  def listActionsWithoutProjectOrMeeting(){
    def currentUser = springSecurityService.currentUser
    if (params.idProject)
      render template:"showActions", model:[actions:Action.findAllByAutorAndProject(currentUser, null, [max:params.max ?: 10, offset:params.offset ?: 0]),
                                            idProject:params.idProject,
                                            total:Action.findAllByAutorAndProject(currentUser, null).size()]
    else
      render template:"showActions", model:[actions:Action.findAllWhere(autor:currentUser,meeting:null),idMeeting:params.idMeeting]
  }

  def listActionsProject(){
    def currentUser = springSecurityService.currentUser
    def project = Project.get(params.id)
    def listActions = Action.findAllByAutorAndProject(currentUser, project, [max:params.max ?: 10, offset:params.offset ?: 0])
    render view:"listActionOfProjectOrMeeting", model:[project:project, 
                                                      total:project.actions.size(),
                                                      listActions:listActions]                   
  }

  def listActionsMeeting(){
    def currentUser = springSecurityService.currentUser
    def meeting = Meeting.get(params.id)
    def listActions = Action.findAllByAutorAndMeeting(currentUser, meeting, [max:params.max ?: 10, offset:params.offset ?: 0])
    render view:"listActionOfProjectOrMeeting", model:[meeting:meeting, 
                                                      total:meeting.actions.size(),
                                                      listActions:listActions]     
  }
  
}
