package com.mymapmanager

@grails.validation.Validateable
class ActionCommand {

  String name 
  String description

  String startingDay
  String finishedDay

  Long idProject
  Long idMeeting

  String participants

  static constraints = {
    name blank:false, size:1..200
    description blank:false, size:1..1000
    startingDay nullable:true
    finishedDay nullable:true
  }

}