package com.mymapmanager

import com.mymapmanager.*

class ContactNetworkController {

  def springSecurityService
  def contactNetworkService

  def index(){
    def currentUser = springSecurityService.currentUser
    [list:currentUser.contactNetwork]
  }

  def findUser(){
    def currentUser = springSecurityService.currentUser
    def message = contactNetworkService.addOneUserToContactNetworkOfCurrentUser(currentUser.id, params.username)
    render template:"yourContacts", model:[list:currentUser.contactNetwork.sort()]
  }

}