package com.mymapmanager

import com.mymapmanager.*
import org.grails.s3.S3Asset

class AttachmentController {

	def  attachmentService

	def addAttachToThisInstance(){
		def clazzIntance = Class.forName(params.clazz).newInstance()
    def instance = clazzIntance.get(params.instanceId)
		def attachmentLink = attachmentService.createAttachmentForThisInstance(instance,params.file)
		render(template:"attachment", model:[instance:instance])
  }

  def deleteAttachToThisInstance(){
    def attachmentLink = AttachmentLink.get(params.attachmentLink)
    def s3Asset = S3Asset.get( params.id )
    attachmentService.deleteAttachFromAttachmentForThisInstance(attachmentLink, s3Asset)  
    def clazzIntance = Class.forName('com.mymapmanager.'+attachmentLink.type).newInstance()
    def instance = clazzIntance.get(attachmentLink.attachmentRef)
    render(template:"attachment", model:[instance:instance])
  }

} 

  