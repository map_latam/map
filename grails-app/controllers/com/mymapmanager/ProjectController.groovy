package com.mymapmanager

import static org.springframework.http.HttpStatus.*

class ProjectController {

  def projectService
  def springSecurityService

  def index(ProjectCommand projectCommand) { 
    [projectCommand:projectCommand]
  }

  def save(ProjectCommand projectCommand){
    def currentUser = springSecurityService.currentUser
    Project project = projectService.createProject(currentUser.id, projectCommand)
    withFormat {
      form multipartForm {
        redirect action:"show", id:project.id
      }
      json {respond project, [status: CREATED] }
    }
  }

  def show(){
    def currentUser = springSecurityService.currentUser
    def project = Project.get(params.id)
    def collaborationLink = CollaborationLink.findByCollaborationRefAndType(project.id,project.class.getSimpleName())
    [project:project,
    collaborationLink:collaborationLink,
    sizeOfActions:project.actions.size(),
    sizeOfMeetings:project.meetings.size(),
    actions:Action.findAllByAutorAndProject(currentUser, null, [max:params.max ?: 10, offset:params.offset ?: 0]),
    idProject:params.idProject,
    total:Action.findAllByAutorAndProject(currentUser, null).size()]
  }

  def list(){
    def currentUser = springSecurityService.currentUser
    def projects = Project.findAllByAutor(currentUser, [max:params.max ?: 10, offset:params.offset ?: 0])
    render view:"list", model:[projects:projects,
                              total:Project.countByAutor(currentUser)]
  }

  def addNotExistingActionToProject(){
    def currentUser = springSecurityService.currentUser
    render template:"project", model:[project:projectService.addNotExistingActionToProject(currentUser.id, params.idProject, params)]
  }

  def addExistingActionToProject(){
    render template:"project", model:[project:projectService.addExistingActionToProject(params.idProject, params.idAction)]
  }

  def addNotExistingMeetingToProject(){
    def currentUser = springSecurityService.currentUser
    render template:"project", model:[project:projectService.addNotExistingMeetingToProject(currentUser.id, params.idProject, params)]
  }

  def addExistingMeetingToProject(){
    render template:"project", model:[project:projectService.addExistingMeetingToProject(params.idProject, params.idMeeting)]
  }

}
