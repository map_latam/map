package com.mymapmanager

class SignUpController {

  def userService
  def springSecurityService

  def index() {
    [userCommand:new UserCommand()]
  }
  def register(UserCommand userCommand){
    if(userCommand.hasErrors()){
      flash.error = g.message(code:'signUp.error',default:'Hey! Review your information...')
      render view:"index", model:[userCommand:userCommand]
    } else {
      try{
        User user = userService.createUserWithUsernamePasswordAndEnable(userCommand.username,userCommand.password)
        springSecurityService.reauthenticate user.username
        redirect controller:"login"
      }catch(Exception e){
        flash.error = e.message
        render view:"index", model:[userCommand:userCommand]
      }
    }
    
  }
}

class UserCommand {
  String username
  String password
  String confirmPassword

  static constraints = {
    username blank: false, email:true
    password blank: false
    confirmPassword blank:false,validator: { val, obj ->
      obj.password == val
    }
  }

  User getUserTransient(){
    new User(username:username,password:password)
  }
}