package com.mymapmanager

import com.mymapmanager.*

class MeetingController {

  def meetingService
  def springSecurityService

  def index() { }

  def save(MeetingCommand meetingCommand){
    def currentUser = springSecurityService.currentUser
    Meeting meeting = meetingService.createMeeting(currentUser.id, meetingCommand)
		redirect action:"show", id:meeting.id
	}

	def show(){
    def meeting = Meeting.get(params.id)
    def collaborationLink = CollaborationLink.findByCollaborationRefAndType(meeting.id,meeting.class.getSimpleName())
    [meeting:meeting,
    collaborationLink:collaborationLink,
    sizeOfActions:meeting.actions.size()]
  }

  def list(){
    def currentUser = springSecurityService.currentUser
    def meetings = Meeting.findAllByAutor(currentUser, [max:params.max ?: 10, offset:params.offset ?: 0])
    render view:"list", model:[meetings:meetings, total:Meeting.countByAutor(currentUser)]
  }

  def listMeetingsWithoutProject(){
    def currentUser = springSecurityService.currentUser
    render template:"showMeetings", model:[meetings:Meeting.findAllWhere(autor:currentUser, project:null),
                                          idProject:params.idProject]
  }

  def listMeetingsOfProject(){
    def currentUser = springSecurityService.currentUser
    def project = Project.get(params.id)
    def listMeetings = Meeting.findAllByAutorAndProject(currentUser, project, [max:params.max ?: 10, offset:params.offset ?: 0])
    render view:"listMeetingOfProject", model:[project:project, 
                                              total:project.meetings.size(),
                                              listMeetings:listMeetings]                   
  }

  def addNotExistingActionToExistingMeeting(){
    def currentUser = springSecurityService.currentUser
    render template:"meeting", model:[meeting:meetingService.addNotExistingActionToExistingMeeting(currentUser.id, params.idMeeting,params)] 
  }

  def addExistingActionToExistingMeeting(){
    render template:"meeting", model:[meeting:meetingService.addExistingActionToExistingMeeting(params.idMeeting,params.idAction)] 
  }

}
