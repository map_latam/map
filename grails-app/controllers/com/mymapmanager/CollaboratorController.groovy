package com.mymapmanager

class CollaboratorController {

  static allowedMethods = [update:'PUT']

  def update() {
    def collaborator = Collaborator.get(params.id)
    collaborator.privilege = params.privileges
    collaborator.save()
    collaborator

    def collaborationLink = CollaborationLink.get(params.collaborationLinkId)

    render template:"/collaborator/participants",
           model:[collaborationLink:collaborationLink,
           instanceClass:collaborationLink.type]
  }
}
