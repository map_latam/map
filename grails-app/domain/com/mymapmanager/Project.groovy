package com.mymapmanager

class Project implements Collaboration, Attachment{

	String name 
	String description

  Date startingDay
  Date finishedDay

	Date dateCreated
  Date lastUpdated

	static constraints = {
		name blank:false, size:1..200
    description blank:false, size:1..1000
    finishedDay nullable:true
	}

  static belongsTo = [autor:User]

	static hasMany = [ meetings : Meeting, actions : Action ]
}
