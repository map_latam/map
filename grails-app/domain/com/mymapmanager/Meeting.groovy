package com.mymapmanager

class Meeting implements Collaboration, Attachment{

	String reason
	String venue

  Date appointmentDay
  Date appointmentDayFinished

	Date dateCreated
  Date lastUpdated

  Project project

	static constraints = {
		reason blank:false, size:1..200
    venue blank:false, size:1..1000 
    appointmentDay blank:false
    appointmentDayFinished nullable:true
    project nullable:true
	} 

  static belongsTo = [autor:User]

	static hasMany = [ actions : Action, topics : Topic ]
}
  