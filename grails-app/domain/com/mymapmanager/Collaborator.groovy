package com.mymapmanager

class Collaborator {

  User user
  PrivilegeType privilege

  Date dateCreated
  Date lastUpdated

  static constraints = {
  }

}