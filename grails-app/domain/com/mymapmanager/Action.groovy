package com.mymapmanager

class Action implements Collaboration, Attachment{

  String name 
  String description

  Date startingDay
  Date finishedDay

  Date dateCreated
  Date lastUpdated

  Project project
  Meeting meeting

  static constraints = {
    name blank:false, size:1..200
    description blank:false, size:1..1000
    project nullable:true
    meeting nullable:true
    startingDay nullable:true
    finishedDay nullable:true
  }

  static belongsTo = [autor:User]


}
