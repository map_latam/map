package com.mymapmanager

class Topic implements Comparable, Collaboration {

  String title
  String description

  Integer duration = 0
  Integer arrange = 1

  Date dateCreated
  Date lastUpdated

  static belongsTo = [meeting:Meeting]

  static constraints = {
    title blank:false, size:1..200
    description nullable:true, blank:true, size:1..1000
    duration min:0
    arrange min:1
  }

  int compareTo(def o) {
    if(this.arrange == o.arrange) {
      return this.id.compareTo(o.id)
    }

    this.arrange.compareTo(o.arrange)
  }

}