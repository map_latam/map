package com.mymapmanager

import org.grails.s3.S3Asset

class AttachmentLink{

	Long attachmentRef
  String type

	static constraints = {
		attachmentRef min:0L
    type blank:false
	}

	static hasMany = [attachments: S3Asset]
}