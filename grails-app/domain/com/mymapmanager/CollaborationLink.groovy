package com.mymapmanager

class CollaborationLink {

  Long collaborationRef
  String type

  static constraints = {
    collaborationRef min:0L
    type blank:false
  }

  static hasMany = [participants : Collaborator]

}