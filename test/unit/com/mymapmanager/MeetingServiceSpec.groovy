package com.mymapmanager

import grails.test.mixin.TestFor
import spock.lang.Specification
import grails.plugin.springsecurity.SpringSecurityService

@TestFor(MeetingService)
@Mock([Meeting,ActionService,Action,User])
class MeetingServiceSpec extends Specification {

  def "Create a new meeting"(){
    given:"""Un usuario en session y recibiendo 
          un meeting command para la creacion de 
          un meeting"""
      User.metaClass.isDirty = { true }
      User.metaClass.encodePassword = {"password"}
      def user = new User(username:"me@me.com").save(validate:false)
      def params=[reason:"reunion", venue:"el lugar es aqui", appointmentDay:"24/04/2014 4:40 PM", appointmentDayFinished:"10:20 PM"]
    and : "y simulamos la colaboración"
      def collaborationServiceMock = mockFor(CollaborationService)
      collaborationServiceMock.demand.inviteParticipantToCollaborateToThisInstance(1){p1,p2,p3 ->}
      service.collaborationService = collaborationServiceMock.createMock()
    when:"""Intentemos crear un meeting con el contenido 
        del meeting command"""
      Meeting meeting = service.createMeeting(user.id, params)
    then:"El id del meeting debera ser mayor que cero, el autor del meeting debera ser el usuario en session"
      meeting.id > 0
      meeting.autor == user
      meeting.reason == "reunion"
      meeting.appointmentDay.format("dd/MM/yyyy h:mm a") == "24/04/2014 4:40 PM"
      meeting.appointmentDayFinished.format("dd/MM/yyyy h:mm a") == "24/04/2014 10:20 PM"
  }

  def "Agregar una accion no existente a una reunion existente"(){
    given:"""Un usuario en session, recibimos la 
          entrada del usuario como un mapa y el 
          id de meeting existente"""
      User.metaClass.isDirty = { true }
      User.metaClass.encodePassword = {"password"}
      def user = new User(username:"me@me.com").save(validate:false)
      def meeting = new Meeting(reason:"reunion", venue:"aqui", appointmentDay:new Date(), autor:user).save(validate:false)
      def params=[name:"accion", description:"description de accion", startingDay:"09/04/2014", finishedDay:"10/04/2014"]

    and : "y simulamos la colaboración"
      def actionServiceMock = mockFor(ActionService)
      actionServiceMock.demand.createAction(1){id, parametros ->
        def action = new Action(parametros)
        action.autor = user
        action
      }
      service.actionService = actionServiceMock.createMock()

    when:"""Intentamos crear una nueva accion y 
        agregarla a la reunion existente"""
      meeting = service.addNotExistingActionToExistingMeeting(user.id, meeting.id, params)
    then:"""El numero de acciones de la reunion deberan de ser 
        uno, el id de la accion creada debera de ser mayor que 
        cero y el autor de la misma debera de ser el usuario
        en session"""
      meeting.actions.size() == 1
      meeting.actions.getAt(0).id > 0
      meeting.actions.getAt(0).autor == user
      meeting.actions.getAt(0).name == "accion"
  }

  def "Agregar una accion existente a una reunion existente"(){
    given:"""Un usuario en session, una reunion 
        existente y una accion existente"""
      def meeting = new Meeting(reason:"reunion", venue:"aqui").save(validate:false)
      def action = new Action(name:"accion", description:"description de accion").save(validate:false)
    when:"""Intentamos agregar la accion existente  
        a la reunion existente"""
      meeting = service.addExistingActionToExistingMeeting(meeting.id,action.id)
    then:"""El numero de acciones de la reunion deberan de ser 
        uno y el nombre de la accion debera corresponder a
        la de la accion existente"""  
      meeting.actions.size() == 1
      meeting.actions.getAt(0).name == "accion"
  }
}