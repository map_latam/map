package com.mymapmanager

import grails.test.mixin.TestFor
import spock.lang.Specification
import grails.test.mixin.Mock
import org.grails.s3.S3Asset
import org.grails.s3.S3AssetService
import org.springframework.mock.web.MockMultipartFile
import com.mymapmanager.*

@TestFor(AttachmentService)
@Mock([Project,AttachmentLink,S3Asset])
class AttachmentServiceSpec extends Specification {

	def "Add one attach to instance that implements attachment"(){
		given:"""One existing project that 
				implements attachment and one 
				multipart file"""
			def project = new Project(name:"project").save(validate:false)
			def file = new MockMultipartFile("file.txt", "", "plain/text", [] as byte[]) 
		and:"Having a mock service of s3AssetService"			
			def s3AssetServiceMock = mockFor(S3AssetService)
			s3AssetServiceMock.demand.getNewTmpLocalFile(1..1) { obj -> return new File("file.txt") }
      s3AssetServiceMock.demand.put(1..1) { S3Asset -> null }
      service.s3AssetService = s3AssetServiceMock.createMock()
		when:"""We try to attach that file to 
         this existing project attachment"""
			def attachmentLink = service.createAttachmentForThisInstance(project, file)
			s3AssetServiceMock.verify()
		then:"""The id of the attachment should be 
         greater than zero, the type of attachment should be 
         project and the size of attachments in the attachmentLink 
         must be one"""
			attachmentLink.id > 0L
			attachmentLink.type=="Project"
			attachmentLink.attachmentRef==1L
			attachmentLink.attachments.size()==1
	}

	def "Add one attach of one instance that not implements attachment"(){
		given:"""One existing anotherItem that not 
				implements attachment and one 
				multipart file"""
			def anotherItem = new AnotherItem()
			def file = new MockMultipartFile("file.txt", "", "plain/text", [] as byte[])
		when:"""We try to attach that file to 
         this existing anotherItem"""
			def attachment = service.createAttachmentForThisInstance(anotherItem, file)
		then:"thrown exception" 
			Exception e = thrown()
      e.message == "Pelaz!"
	}

	def "Add one attach of a instance that existing right"(){
		given:""
			def project = new Project(name:"project").save(validate:false)
			def file = new MockMultipartFile("file.txt", "", "plain/text", [] as byte[]) 
			def attachmentLink = new AttachmentLink(attachmentRef:project.id,type:project.class.getSimpleName())
			def receipt = new S3Asset().save(validate:false)
			attachmentLink.addToAttachments(receipt).save()
		and:"Having a mock service of s3AssetService"			
			def s3AssetServiceMock = mockFor(S3AssetService)
			s3AssetServiceMock.demand.getNewTmpLocalFile(1..1) { obj -> return new File("file.txt") }
      s3AssetServiceMock.demand.put(1..1) { S3Asset -> null }
      service.s3AssetService = s3AssetServiceMock.createMock()
		when:""
			attachmentLink = service.createAttachmentForThisInstance(project, file)
			s3AssetServiceMock.verify()
		then:""
			attachmentLink.id > 0L
			attachmentLink.type=="Project"
			attachmentLink.attachmentRef==1L
			attachmentLink.attachments.size()==2
	}

	def "Delete an attach of one existing instance that implements attachment"(){
		given:"One instance of project, attachmentLink than have one attach"
			def project = new Project(name:"project").save(validate:false)
			def attachmentLink = new AttachmentLink(attachmentRef:project.id,type:project.class.getSimpleName()).save(validate:false)
			def receipt = new S3Asset().save(validate:false)
			attachmentLink.addToAttachments(receipt).save()
		and:"Having a mock service of s3AssetService"			
			def s3AssetServiceMock = mockFor(S3AssetService)
      s3AssetServiceMock.demand.delete(1..1) { S3Asset -> true }
      service.s3AssetService = s3AssetServiceMock.createMock()
		when:""
			attachmentLink = service.deleteAttachFromAttachmentForThisInstance(attachmentLink, receipt)
			s3AssetServiceMock.verify()
		then:""
			attachmentLink.id > 0L
			attachmentLink.attachments.size()==0

	}

}