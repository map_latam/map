package com.mymapmanager

import grails.test.mixin.TestFor
import spock.lang.Specification
import grails.plugin.springsecurity.SpringSecurityService

@TestFor(TopicService)
@Mock([Topic,Meeting,User])
class TopicServiceSpec extends Specification {

  def "Create a new project"(){
    given:"Un tema ya creado"
      Topic topic = new Topic(title:"algodon")
      topic.save(validate:false)
      def params=[ idTopic : 1, title : "Algo", duration : 0, arrange : 1, description : "description"]
      def username = "lol@lulz.com"

    and : "y simulando la colaboración"
      def collaborationServiceMock = mockFor(CollaborationService)
      collaborationServiceMock.demand.inviteParticipantToCollaborateToThisInstance(1){p1,p2,p3 ->}
      service.collaborationService = collaborationServiceMock.createMock()

    when:"Actualizamos ese tema en específico"
      def updated = service.update(username, params)
      collaborationServiceMock.verify()

    then:"Verificamos que los elementos se ejecuten de manera adecuada"
      updated.id == 1
      updated.title == "Algo"
      updated.duration == 0
      updated.arrange == 1
      updated.description == "description"
  }
}
