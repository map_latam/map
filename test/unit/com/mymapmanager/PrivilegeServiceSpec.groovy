package com.mymapmanager

import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(PrivilegeService)
@Mock([Action, CollaborationLink, User, Collaborator])
class PrivilegeServiceSpec extends Specification {

  def "Change privilege to a participant of a collaboration element"() {
    given :
      Action instance = new Action()
      instance.save(validate:false)

      def username = "currentUser@map.com"
      User.metaClass.isDirty = { true }
      User.metaClass.encodePassword = {"password"}
      def currentUser = new User(username:username).save(validate:false)

      def collaborationLink = new CollaborationLink(
        collaborationRef:instance.id,
        type:instance.class.getSimpleName())
      collaborationLink.addToParticipants(
        new Collaborator(user:currentUser,privilege:PrivilegeType.READ)
      )
      collaborationLink.save()

    when :
      def result = service.assignPrivilegeToParticipantInInstance(PrivilegeType.WRITE, username, instance)

    then :
      result instanceof CollaborationLink
      result.id > 0
      result.collaborationRef == instance.id
      result.type == instance.class.getSimpleName()
      result.participants.size() == 1
      result.participants[0].user.id == currentUser.id
      result.participants[0].privilege == PrivilegeType.WRITE
  }

  def "Throw exception when the instance sent does not implement Collaboration"() {
    given :
      Object instance = new Object()

    when :
      service.assignPrivilegeToParticipantInInstance(PrivilegeType.READ, "", instance)

    then :
    def e = thrown(RuntimeException)
    e.message == "Class does not implement Collaboration"
  }

}
