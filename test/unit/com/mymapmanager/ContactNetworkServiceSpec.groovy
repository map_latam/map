package com.mymapmanager

import grails.test.mixin.TestFor
import spock.lang.Specification
import grails.plugin.springsecurity.SpringSecurityService

@TestFor(ContactNetworkService)
@Mock([User])
class ContactNetworkServiceSpec extends Specification {

  def "Agregar un usuario existente a mi red de contactos"(){
    given:"""Dado que tengo un usuario en la app, 
          y otro usuario que ya existe en la app 
          que quiero agregar a mi red de contactos"""
      User.metaClass.isDirty = { true }
      User.metaClass.encodePassword = {"password"}
      def currentUser = new User(username:"currentUser@map.com").save(validate:false)
      def anotherUser = new User(username:"anotherUser@map.com").save(validate:false)
      def emailAnotherUser = "anotherUser@map.com"
    and:"We simulate the user creation"
      def userServiceMock = mockFor(UserService)
      userServiceMock.demand.createUserWithUsernamePasswordAndEnable { u,p,e ->
        new User(username:"anotherUser@map.com")
      }
      service.userService = userServiceMock.createMock()
    when:"Intentamos agregar el usuario existente a la red de contactos del usuario actual"
      service.addOneUserToContactNetworkOfCurrentUser(currentUser.id, emailAnotherUser)
    then:"""El tamaño de la red de contactos 
          debe incrementar en uno y corresponder el 
          último usuario agregado al existente"""
      currentUser.contactNetwork.size() == 1
      currentUser.contactNetwork.getAt(0).username == "anotherUser@map.com"
      currentUser.contactNetwork.getAt(0).contactNetwork.size() == 1
      currentUser.contactNetwork.getAt(0).contactNetwork.getAt(0).username == "currentUser@map.com"
  }

  def "Agregar un usuario no existente a mi red de contactos "(){
    given:"""Dado que tengo un usuario en la app, 
          y otro usuario que no existe en la app 
          que quiero agregar a mi red de contactos"""
      User.metaClass.isDirty = { true }
      User.metaClass.encodePassword = {"password"}
      def currentUser = new User(username:"currentUser@map.com").save(validate:false)
      def emailAnotherUser = "anotherUser@map.com"
    and:"We simulate the user creation"
      def userServiceMock = mockFor(UserService)
      userServiceMock.demand.createUserWithUsernamePasswordAndEnable { u,p,e ->
        new User(username:emailAnotherUser,enabled:false).save(validate:false)
      }
      service.userService = userServiceMock.createMock()
    when:"Intentamos agregar el usuario no existente a la red de contactos del usuario actual"
      service.addOneUserToContactNetworkOfCurrentUser(currentUser.id, emailAnotherUser)
    then:"""El tamaño de la red de contactos 
          debe incrementar en uno, el id del usuario creado
          debe de ser mayor que cero, estar inhabilitado
          y corresponder el último usuario agregado al existente"""
      currentUser.contactNetwork.size() == 1
      currentUser.contactNetwork.getAt(0).id > 0
      currentUser.contactNetwork.getAt(0).enabled == false
      currentUser.contactNetwork.getAt(0).username == "anotherUser@map.com"
      currentUser.contactNetwork.getAt(0).contactNetwork.size() == 1
      currentUser.contactNetwork.getAt(0).contactNetwork.getAt(0).username == "currentUser@map.com"
  }

    def "No agreagar un usuario exitente a mi red de contacos si ya lo tengo en mi red"(){
      given:"""Dado que tengo un usuario en la app, 
          y otro usuario que ya existe en la app 
          y que ya se encuentran en la red de contacto"""
        User.metaClass.isDirty = { true }
        User.metaClass.encodePassword = {"password"}
        def currentUser = new User(username:"currentUser@map.com").save(validate:false)
        def anotherUser = new User(username:"anotherUser@map.com").save(validate:false)
        currentUser.addToContactNetwork(anotherUser)
        anotherUser.addToContactNetwork(currentUser)
        def emailAnotherUser = "anotherUser@map.com"
      when:"Intentamos agregar el usuario existente a la red de contactos del usuario actual"
        service.addOneUserToContactNetworkOfCurrentUser(currentUser.id, emailAnotherUser)
      then:"""El tamaño de la red de contactos 
          permanecer en uno tanto del usuario
          actual como del otro usuario"""
        currentUser.contactNetwork.size() == 1
        currentUser.contactNetwork.getAt(0).contactNetwork.size() == 1

  }

}