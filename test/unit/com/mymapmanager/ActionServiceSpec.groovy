package com.mymapmanager

import grails.test.mixin.TestFor
import spock.lang.Specification
import grails.plugin.springsecurity.SpringSecurityService

@TestFor(ActionService)
@Mock([Action,User])
class ActionServiceSpec extends Specification {

  def "Create a new action"(){
    given:"""A user in session and receiving 
         one action command for creating 
         a action"""
      User.metaClass.isDirty = { true }
      User.metaClass.encodePassword = {"password"}
      def user = new User(username:"me@me.com").save(validate:false)
      def actionCommand=[name:"accion", description:"description de accion", startingDay:"09/04/2014", finishedDay:"10/04/2014"]
    and : "y simulamos la colaboración"
      def collaborationServiceMock = mockFor(CollaborationService)
      collaborationServiceMock.demand.inviteParticipantToCollaborateToThisInstance(1){p1,p2,p3 ->}
      service.collaborationService = collaborationServiceMock.createMock()
    when:"""Try to create an action with the content 
        the action command"""
      Action action = service.createAction(user.id, actionCommand)
    then:"""The id of the action must be greater than cero 
         the size of user actions should be 
         one, the author of the action should be the user 
         in session"""
      action.id > 0
      user.actions.size() == 1
      action.autor == user
      action.name == "accion"
      action.startingDay.format("dd/MM/yyyy") == "09/04/2014"
  }
}