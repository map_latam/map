package com.mymapmanager

import grails.test.mixin.TestFor
import spock.lang.Specification
import grails.plugin.springsecurity.SpringSecurityService

@TestFor(ProjectService)
@Mock([Project,Action,Meeting,ActionService,MeetingService,User])
class ProjectServiceSpec extends Specification {

  def "Create a new project"(){
    given:"""Un usuario en session y recibiendo 
          un project command para la creacion de 
          un project"""
      User.metaClass.isDirty = { true }
      User.metaClass.encodePassword = {"password"}
      def user = new User(username:"me@me.com").save(validate:false)
      def params=[name:"proyecto", description:"description de proyecto", startingDay:"09/04/2014", finishedDay:"10/04/2014"]
    and : "y simulamos la colaboración"
      def collaborationServiceMock = mockFor(CollaborationService)
      collaborationServiceMock.demand.inviteParticipantToCollaborateToThisInstance(1){p1,p2,p3 ->}
      service.collaborationService = collaborationServiceMock.createMock()
    when:"""Intentemos crear un project con el contenido 
        del project command"""
      Project project = service.createProject(user.id, params)
      collaborationServiceMock.verify()
    then:"""El id del project debera ser mayor que 0
        el nuemero de projects del usuario debera de ser
        uno, el autor del project debera ser el usuario 
        en session"""
      user.projects.size() == 1
      user.projects.getAt(0).name == "proyecto"
      project.id > 0
      project.autor == user
      project.name == "proyecto"
      project.startingDay.format("dd/MM/yyyy") == "09/04/2014"
  }

  def "Agregar una accion a un proyecto que ya existe"(){
    given:"""Un usuario en session, una accion
        existente y un proyecto existente"""
      def project = new Project(name:"proyecto", description:"description").save(validate:false)
      def action = new Action(name:"accion", description:"description de accion").save(validate:false)
    when:"""Intentamos agregar la accion existente  
        a el proyecto existente"""
      service.addExistingActionToProject(project.id, action.id)
    then:"""El numero de acciones del proyecto deberan de ser 
        uno y el nombre de la accion debera corresponder a
        la de la accion existente"""  
      project.actions.size()==1
      project.actions.getAt(0).name=="accion"
      project.actions.getAt(0).project==project
  }

  def "Agregar una accion no existente a un proyecto que ya existe"(){
    given:"""Un usuario en session, recibimos la 
          entrada del usuario como un mapa y el 
          id de project existente"""
      User.metaClass.isDirty = { true }
      User.metaClass.encodePassword = {"password"}
      def user = new User(username:"me@me.com").save(validate:false)
      def project = new Project(name:"proyecto", description:"description", startingDay:new Date(), autor:user).save(validate:false)
      def params=[name:"accion", description:"description de accion", startingDay:"09/04/2014", finishedDay:"10/04/2014"]

    and : "y simulamos la colaboración"
      def actionServiceMock = mockFor(ActionService)
      actionServiceMock.demand.createAction(1){id, parametros ->
        def action = new Action(parametros)
        action.autor = user
        action
      }
      service.actionService = actionServiceMock.createMock()

    when:"""Intentamos crear una nueva accion y 
        agregarla al proyecto existente"""
      service.addNotExistingActionToProject(user.id, project.id, params)
    then:"""El numero de acciones del proyecto deberan de ser 
        uno, el id de la accion creada debera de ser mayor que 
        cero y el autor de la misma debera de ser el usuario
        en session"""
      project.actions.size()==1
      project.actions.getAt(0).id > 0
      project.actions.getAt(0).autor == user
      project.actions.getAt(0).name=="accion"
      project.actions.getAt(0).project==project
  }

  def "Agregar una minuta a un proyecto que ya existe"(){
    given:"""Un usuario en session, una reunion 
        existente y un proyecto existente"""
      def project = new Project(name:"proyecto", description:"description").save(validate:false)
      def meeting = new Meeting(reason:"minuta", venue:"lugar de la minuta").save(validate:false)
    when:"""Intentamos agregar la minuta existente  
        a el proyecto existente"""
      service.addExistingMeetingToProject(project.id, meeting.id)
    then:"""El numero de minutas del proyecto deberan de ser 
        uno el proyecto de la reunion debera de ser el mismo
        proyecto"""  
      meeting.project == project
      project.meetings.size()==1
      project.meetings.getAt(0).reason=="minuta"
      project.meetings.getAt(0).project==project
  }

  def "Agregar una minuta no existente a un proyecto que ya existe"(){
    given:"""Un usuario en session, recibimos la 
          entrada del usuario como un mapa y el 
          id de proyecto existente"""
      User.metaClass.isDirty = { true }
      User.metaClass.encodePassword = {"password"}
      def user = new User(username:"me@me.com").save(validate:false)
      def project = new Project(name:"proyecto", description:"description", startingDay:new Date(), autor:user).save(validate:false)
      def params = [reason:"reunion", venue:"el lugar es aqui", appointmentDay:"24/04/2014 4:40 PM", appointmentDayFinished:"10:20 PM"]

    and : "y simulamos la colaboración"
      def meetingServiceMock = mockFor(ActionService)
      meetingServiceMock.demand.createMeeting(1){id, parametros ->
        def meeting = new Meeting(parametros)
        meeting.autor = user
        meeting
      }
      service.meetingService = meetingServiceMock.createMock()

    when:"""Intentamos crear una nueva reunion y 
        agregarla al proyecto existente"""
      service.addNotExistingMeetingToProject(user.id, project.id, params)
    then:"""El tamaño de las reuniones del proyecto debra de
        ser uno, el id de la reunion creada debera de ser
        mayor que cero"""
      project.meetings.size()==1
      project.meetings.getAt(0).id > 0
      project.meetings.getAt(0).autor == user
      project.meetings.getAt(0).reason == "reunion"
      project.meetings.getAt(0).project == project
  }

}