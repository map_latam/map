package com.mymapmanager

import grails.test.mixin.TestFor
import spock.lang.Specification
import grails.test.mixin.Mock
import com.mymapmanager.*
import grails.plugin.springsecurity.SpringSecurityService

@TestFor(CollaborationService)
@Mock([Project,CollaborationLink,User,Collaborator])
class CollaborationServiceSpec extends Specification {

  def """Invite to an existing user to collaborate 
       in a project where no invited users"""(){
    given:"""A user in session, having existing
          project without collaboration and a user 
          to add to an existing collaboration"""
      User.metaClass.isDirty = { true }
      User.metaClass.encodePassword = {"password"}
      def currentUser = new User(username:"currentUser@map.com").save(validate:false)
      def user = new User(username:"user@map.com").save(validate:false)
      def project = new Project(name:"proyecto prueba").save(validate:false)
      def emailUser = "user@map.com"
    and:"""Having a mock service of contactNetworkService"""
      def contactNetworkServiceMock = mockFor(ContactNetworkService)
      contactNetworkServiceMock.demand.addOneUserToContactNetworkOfCurrentUser(1..1){obj -> return true }
      service.contactNetworkService  = contactNetworkServiceMock.createMock()
    when:"""We try to add that user to 
         this existing project collaboration"""
      def collaborationLink = service.inviteParticipantToCollaborateToThisInstance(project, currentUser, emailUser)
      contactNetworkServiceMock.verify()
    then:"""The id of the collaboration should be 
         greater than zero, the type of collaboration should be 
         project and the size of participants in the collaboration 
         must be one"""
      collaborationLink.id > 0
      collaborationLink.type == "Project"
      collaborationLink.collaborationRef == 1L
      collaborationLink.participants.size() == 1
      collaborationLink.participants.getAt(0).user.username == "user@map.com"
  }

  def """Invite three existing users to collaborate on 
       a project where there are no invited users"""(){
    given:"""A user session, taking existing project without 
        collaboration and three existing users to add to a 
        collaboration"""
      User.metaClass.isDirty = { true }
      User.metaClass.encodePassword = {"password"}
      def currentUser = new User(username:"currentUser@map.com").save(validate:false)
      def user1 = new User(username:"user1@map.com").save(validate:false)
      def user2 = new User(username:"user2@map.com").save(validate:false)
      def user3 = new User(username:"user3@map.com").save(validate:false)
      def project = new Project(name:"proyecto prueba").save(validate:false)
      def emailUsers = "user1@map.com, user2@map.com, user3@map.com"
    and:"""Having a mock service of contactNetworkService"""
      def contactNetworkServiceMock = mockFor(ContactNetworkService)
      contactNetworkServiceMock.demand.addOneUserToContactNetworkOfCurrentUser(1..3){obj1, obj2-> return true }
      service.contactNetworkService  = contactNetworkServiceMock.createMock()
    when:"""We try to add two users to the 
        collaboration of the existing project"""
      def collaborationLink = service.inviteParticipantToCollaborateToThisInstance(project, currentUser, emailUsers)
      contactNetworkServiceMock.verify()
    then:"""The id of the collaboration should be 
         greater than zero, the type of collaboration should be 
         project and the size of participants in the collaboration 
         should be 3"""
      collaborationLink.id > 0
      collaborationLink.type == "Project"
      collaborationLink.collaborationRef == 1L
      collaborationLink.participants.size() == 3
      collaborationLink.participants.getAt(0).user.username == "user1@map.com"
      collaborationLink.participants.getAt(1).user.username == "user2@map.com"
      collaborationLink.participants.getAt(2).user.username == "user3@map.com"
  } 

}