package com.mymapmanager

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll


@TestFor(UserService)
@Mock([Role,User,UserRole])
class UserServiceSpec extends Specification {

  @Unroll("Create a user with username '#username', password '#password' and is enabled '#enabled'")
  void "Create a user and assign role into the system"() {
    given:"Simulate the encoding password and existing roles"
      User.metaClass.encodePassword = { delegate.password = "1234567890" }
      new Role(authority:"ROLE_USER").save()
    when:"We save the user into the system"
      User user = service.createUserWithUsernamePasswordAndEnable(username,password,enabled)
    then:"We have a new user created with the lowest role and encoding password"
      user.id > 0
      user.username == "anyuser@anydomain.com"
      user.password != "password"
      user.authorities[0].authority == "ROLE_USER"
      user.enabled == enabled
    where:
      username                | password   | enabled
      "anyuser@anydomain.com" | "password" | true
      "anyuser@anydomain.com" | "password" | false
  }

  @Unroll("SignUp an existing, disabled and invited user '#_username' with password '#_password'")
  void  "SignUp an existing and invited user"() {
    given:"Simulate the encoding password and existing roles, and a existing user with role"
      User.metaClass.encodePassword = { delegate.password = "asdfghjkl" }
      new Role(authority:"ROLE_USER").save()
      def existingDisabledUser = new User(
        username:_username,
        password:_password,
        enabled:false).save()
      UserRole.create existingDisabledUser, Role.findByAuthority("ROLE_USER")
    when:"We sign up the existing user"
      User user = service.createUserWithUsernamePasswordAndEnable(_username,_password,true)
    then:"The user must be enabled and the password must be set"
      user.username == _username
      user.password == "asdfghjkl"
      user.authorities[0].authority == "ROLE_USER"
      user.enabled
    where:
      _username                  | _password   
      "userExist@anydomain.com"  | "userExist@anydomain.com"
  }

  def "Send error if the user already exists and wants to sign up again"(){
    given:"An existing and enabled user"
    User.metaClass.encodePassword = { delegate.password = "asdfghjkl" }
    new Role(authority:"ROLE_USER").save()
    def existingUser = new User(
        username:"userExist@anydomain.com",
        password:"asdfghjkl",
        enabled:true).save()
      UserRole.create existingUser, Role.findByAuthority("ROLE_USER")
    when:"We sign up the existing user"
    service.createUserWithUsernamePasswordAndEnable("userExist@anydomain.com","asdfghjkl",true)
    then:"We receive an error because already exists"
      def e = thrown(Exception)
      e.message == "The user 'userExist@anydomain.com' already exists..."
  }
}