$( "#panelParticipants" ).on( "click", "ul.todo-list i[privileges]", function() {
  var params = {
    privileges : $(this).attr('privileges'),
    collaborationLinkId: $(this).attr('collaborationLinkId')
  };

  var url = $('#updatePrivileges').val() + "/" + $(this).attr('id')

  $.ajax({
    method: "PUT",
    url: url,
    contentType:'application/x-www-form-urlencoded',
    data: params
  }).done(function( data ) {
    console.log(data)
    $('#panelParticipants').html(data);
  }).fail(function(jqXHR, textStatus ){
    console.log( jqXHR.responseText );
    console.log( jqXHR.status );
  });
});