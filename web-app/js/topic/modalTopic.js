$('#topicFormModal').modal({
  show : false
});

$('#topics').on('click', 'a.topics', function(event){
  event.preventDefault();
  $('#topicFormModal').modal('show');
  var topicUrlToShow = $(this).attr('href');

  $.get(topicUrlToShow, function(data) {
    $('#topicFormModal div.modal-content').html(data);

    $("textArea#fullFormDescription").wysihtml5({
      "stylesheets": [$("textArea[css-url]").first().attr('css-url')],
      "html":true,
      "color": true,
      "link": true, 
      "image": false,
      "indent":false
    });

    tagitInitialize();
  });
});

modalHide = function() {
  $('#topicFormModal').modal('hide');
};

cleanBasicForm = function(){
  $('#formTopic #title').val('');
}