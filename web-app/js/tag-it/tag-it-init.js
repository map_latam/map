tagitInitialize = function() {
  $("input[name=participants]")
    .tagit({beforeTagAdded: function(event, ui) {
      var patronValidacion = /\S+@\S+\.\S+/
      if (!patronValidacion.test(ui.tagLabel)) {
        $("#message").show()
        $("#participants").addClass("has-error has-feedback");
        return false
      }else{
        $("#message").hide()
        $("#participants").removeClass("has-error has-feedback");
      }
    }
  });
}

tagitInitialize();