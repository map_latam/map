$(function(){
  $("textArea[name='description']").wysihtml5({
    "stylesheets": [$("textArea[css-url]").first().attr('css-url')],
    "html":true,
    "color": true,
    "link": true, 
    "image": false,
    "indent":false
  });
});