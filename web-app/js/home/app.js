jQuery(function ($) {
  'use strict';

  window.App = {
    init: function () {
      this.initActionWidget();
      this.initMeetingWidget();
      this.initProjectWidget();
    },
    initActionWidget : function(){
      var selectors = {
        actionFormSelector : '#quick-action',
        template : "#actions-template",
        actionListUrl : $("#action-list").val(),
        actionPostUrl : $("#action-post").val(),
        actionListViewSelector : "#my-actions",
        classCountUpdate : '.actionCount'
      };
      this.actions = new Actions(selectors);
      this.actions.getCurrentActions();
    },
    initProjectWidget : function(){
      var selectors = {
        projectPostUrl:$("#project-post").val(),
        projectEditUrl : $("#project-edit").val(),
        projectShowUrl : $("#project-show").val(),
        projectSelector : '#new-project',
        projectNameSelector : 'input[name=name]',
        projectDescriptionSelector : 'textarea[name=description]',
        projectStartingDaySelector : '#startingDay',
        projectFinishedDaySelector : '#finishedDay',
        projectParticipantsSelectorClear : 'input[name=participants]',
        projectParticipantsSelectorValue : '#inputError2',
        classProjectCount : '.projectCount'
      };
      this.projectView = new ProjectView(selectors);
    },
    initMeetingWidget : function(){
      var selectors = {
        meetingFormSelector : "#meetingForm",
        appointmentDaySelector : "#appointmentDay",
        startingHourSelector : "#startingHour",
        endingHourSelector : "#endingHour",
        durationsSelector : "#durations"
      };
      this.meetingController = new MeetingController(selectors);
    }
  };

  App.init();
});
