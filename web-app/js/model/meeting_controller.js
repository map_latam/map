'use strict';

window.MeetingController = (function() {

  MeetingController.prototype.template = "";
  MeetingController.prototype.formSelector = "";

  MeetingController.prototype.appointmentDaySelector = "";
  MeetingController.prototype.startingHourSelector = "";
  MeetingController.prototype.endingHourSelector = "";
  MeetingController.prototype.durationsSelector = "";
  MeetingController.prototype.content = null;

  function MeetingController(selectors) {
    var now = moment();
    this.content = new Meeting(now);
    $(".calendar").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    $(".time").inputmask("hh:mm", {"placeholder": "hh:mm"});
    this.formSelector = selectors.meetingFormSelector;
    this.appointmentDaySelector = selectors.appointmentDaySelector;
    this.startingHourSelector = selectors.startingHourSelector;
    this.endingHourSelector = selectors.endingHourSelector;
    this.durationsSelector = selectors.durationsSelector;
    $(this.formSelector).on('submit', this.processMeeting.bind(this));
    $(this.durationsSelector).on('click', this.setDuration.bind(this));
    this.render();
  }

  MeetingController.prototype.processMeeting = function(e) {
    console.log("Process meeting");
    e.preventDefault();
  };

  MeetingController.prototype.setDuration = function(e) {
    console.log("Setting duration");
    console.log($(e.target).val());
    e.preventDefault();
  };

  MeetingController.prototype.render = function(e) {
    $(this.appointmentDaySelector).val( this.content.appointmentDay );
    $(this.startingHourSelector).val( this.content.startTime );
    $(this.endingHourSelector).val( this.content.endTime );
  };

  return MeetingController;

})();