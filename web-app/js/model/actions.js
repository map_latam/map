'use strict';

var ENTER_KEY = 13;
var ESCAPE_KEY = 27;

window.Actions = (function() {
  Actions.prototype.content = [];
  Actions.prototype.classCountUpdate = '';
  Actions.prototype.template = "";
  Actions.prototype.actionListUrl = "";
  Actions.prototype.actionPostUrl = "";
  Actions.prototype.actionFormSelector = "";
  Actions.prototype.actionListViewSelector = "";

  function Actions(selectors){
    this.actionFormSelector = selectors.actionFormSelector;
    this.classCountUpdate = selectors.classCountUpdate;
    this.template = selectors.template;
    this.actionListUrl = selectors.actionListUrl;
    this.actionPostUrl = selectors.actionPostUrl;
    this.actionListViewSelector = selectors.actionListViewSelector;

    this.actionInput = $(this.actionFormSelector).find("input");
    this.actionInput.on('keyup', this.addActionWithKeyboard.bind(this));
    this.actionButton = $(this.actionFormSelector).find("button").parent();
    this.actionButton.on('click','button', this.addActionWithClick.bind(this));
  }

  Actions.prototype.addActionWithClick = function(e){
    var actionNameInput = this.actionInput.val()
    if (actionNameInput.length < 5) {
      return;
    }
    this.addAction(actionNameInput);
    this.actionInput.val('');
  };

  Actions.prototype.addActionWithKeyboard = function(e){
    var that = this;
    var $input = $(e.target);
    var actionNameInput = $input.val().trim();
    if (e.which !== ENTER_KEY || !actionNameInput) {
      return;
    }
    if (actionNameInput.length < 5) {
      return;
    }
    this.addAction(actionNameInput);
    $input.val('');
  };

  Actions.prototype.getCurrentActions = function(){
    var actionListUrl = this.actionListUrl;
    var that = this;
    that.content = [];
    $.getJSON( actionListUrl, function( data ) {
      $.each( data, function( index, actionRaw ) {
        that.content.push(Action.createFromJson(actionRaw));
      });
    }).done(function() {
      that.render(that.content);
    })
    .fail(function(e) {
      console.log( "error" );
    })
    .always(function() {
    });
    return this.content;
  };

  Actions.prototype.addAction = function(actionName){
    var that = this;
    var action = new Action();
    action.id = 0;
    action.name = actionName;
    action.description = actionName;
    $.post(this.actionPostUrl, action)
    .done(function(data) {
      if(that.content.length > 9)
        that.content.pop();
      that.content.unshift(Action.createFromJson(data));
      that.addOneToCounter();
      that.render();
    })
    .fail(function(e) {
      console.log( "error" );
    })
    .always(function() {
    });
  };

  Actions.prototype.render = function(){
    var source = $(this.template).html();
    var template = Handlebars.compile(source);
    var html = template(this.content);
    $(this.actionListViewSelector).html(html);
  };

  Actions.prototype.addOneToCounter = function(){
    var counter = $(this.classCountUpdate).text();
    counter  = (counter * 1)+ 1;
    $(this.classCountUpdate).text(counter);
  };

  return Actions;

})();
