'use strict';

window.ProjectView = (function(){

  ProjectView.prototype.projectEditUrl = "";
  ProjectView.prototype.projectPostUrl = "";
  ProjectView.prototype.projectShowUrl = "";
  ProjectView.prototype.projectSelector = "";
  ProjectView.prototype.projectNameSelector = "";
  ProjectView.prototype.projectDescriptionSelector = "";
  ProjectView.prototype.projectStartingDaySelector = "";
  ProjectView.prototype.projectFinishedDaySelector = "";
  ProjectView.prototype.projectParticipantsSelectorClear = "";
  ProjectView.prototype.projectParticipantsSelectorValue = "";
  ProjectView.prototype.classProjectCount = "";


  function ProjectView(selectors){
    $(selectors.projectSelector).find('.alert-success').hide();
    var now = moment();
    this.project = new Project(now);

    this.projectSelector = selectors.projectSelector;
    this.projectEditUrl = selectors.projectEditUrl;
    this.projectPostUrl = selectors.projectPostUrl;
    this.projectShowUrl = selectors.projectShowUrl;
    this.classProjectCount = selectors.classProjectCount;
    this.projectNameSelector = selectors.projectNameSelector;
    this.projectDescriptionSelector = selectors.projectDescriptionSelector;
    this.projectStartingDaySelector = selectors.projectStartingDaySelector;
    this.projectFinishedDaySelector = selectors.projectFinishedDaySelector;
    this.projectParticipantsSelectorClear = selectors.projectParticipantsSelectorClear;
    this.projectParticipantsSelectorValue = selectors.projectParticipantsSelectorValue;

    var projectEdit = $(this.projectSelector).find("button").parent();
    projectEdit.on('click','#edit',this.editAdvanced.bind(this));  
    var createProject = $(this.projectSelector).find("button").parent();
    projectEdit.on('click','#create',this.addProject.bind(this));
    var inputProject = $(this.projectDescriptionSelector);
    inputProject.on('keyup',this.counter.bind(this)); 
    this.render();
  };

  ProjectView.prototype.editAdvanced = function(){
    var data = $(this.projectSelector).find(".box-body").find("input").add("textarea").serialize()
    window.location = this.projectEditUrl+data;
  };

  ProjectView.prototype.counter = function(e){
    var $textarea = $(e.target);
    var counterTextArea = $textarea.val().trim();
    $("#counter").text(counterTextArea.length);
    if(counterTextArea.length > 100 && counterTextArea.length < 200){
      $("#left-counter p").addClass("text-primary");
    }else{
      $("#left-counter p").removeClass("text-primary");
    }
    if(counterTextArea.length >= 200){
      $("#left-counter p").addClass("text-danger");
      $textarea.val($textarea.val().substr(0,200));
      $("#counter").text($textarea.val().length);
    }else{
      $("#left-counter p").removeClass("text-danger");
    }
  };

  ProjectView.prototype.addProject = function(e){
    var that = this;
    this.project.id=0;
    this.project.name=$(this.projectNameSelector).val();
    if ($(this.projectDescriptionSelector).val().trim() == "")
      this.project.description="No description"
    else
      this.project.description=$(this.projectDescriptionSelector).val();
    this.project.startingDay=$(this.projectStartingDaySelector).val();
    this.project.finishedDay=$(this.projectFinishedDaySelector).val();
    this.project.participants=$(this.projectParticipantsSelectorValue).val();

    $.post(this.projectPostUrl, this.project)
      .done(function(data) {
        that.clearFormProject();
        that.addOneToCounter();
        var project = Project.createFromJson(data);
        $(that.projectSelector).find('span.alert-body')
          .html('<p><a class="alert-link" href='+that.projectShowUrl+project.id+'><i class="fa fa-info-circle"></i> You create a new project <span class="badge bg-green"> '+project.name+'</span>, click for more detail.</a></p>');
        $(that.projectSelector).find('.alert-success').fadeIn();
      })
      .fail(function(e) {
        console.log( "error" );
      })
      .always(function() {
      });

      return this.project
  };

  ProjectView.prototype.clearFormProject = function(){
    $(this.projectNameSelector).val('');
    $(this.projectStartingDaySelector).val('');
    $(this.projectFinishedDaySelector).val('');
    $(this.projectDescriptionSelector).val('');
    $(this.projectParticipantsSelectorClear).tagit('removeAll');
  };

  ProjectView.prototype.addOneToCounter = function(){
    var counter = $(this.classProjectCount).text();
    counter  = (counter * 1)+ 1;
    $(this.classProjectCount).text(counter);
  };

  ProjectView.prototype.render = function() {
    $(this.projectStartingDaySelector).val(this.project.startingDay);
    $(this.projectFinishedDaySelector).val(this.project.finishedDay);
  };

  return ProjectView;

})();
