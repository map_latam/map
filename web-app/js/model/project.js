'use strict';

window.Project = (function(){

  Project.prototype.id = 0;
  Project.prototype.name = '';
  Project.prototype.description = '';
  Project.prototype.startingDay = '';
  Project.prototype.finishedDay = '';
  Project.prototype.participants = '';

  function Project(moment){
    if(moment) {
      this.startingDay = moment.format("D/MM/YYYY");
      this.finishedDay = moment.add('days', 1).format("D/MM/YYYY");
    }
  }

  Project.prototype.method = function(projectRaw) {
  };

  Project.createFromJson = function(projectRaw){
    var project = new Project();
    project.id = projectRaw.id;
    project.name = projectRaw.name;
    project.description = projectRaw.description;
    project.startingDay = projectRaw.startingDay;
    project.finishedDay = projectRaw.finishedDay;
    return project
  };

  return Project;

})();
