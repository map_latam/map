'use strict';

window.Action = (function() {

  Action.prototype.id = 0;
  Action.prototype.name = '';
  Action.prototype.description= '';
  Action.prototype.startingDay = '';
  Action.prototype.finishedDay = '';

  function Action() {
  }

  Action.prototype.method = function(actionRaw) {
  };
  Action.createFromJson = function(actionRaw) {
    var action = new Action();
    action.id = actionRaw.id;
    action.name = actionRaw.name;
    action.description = actionRaw.description;
    action.startingDay = actionRaw.startingDay;
    action.finishedDay = actionRaw.finishedDay;
    return action;
  };

  return Action;

})();

