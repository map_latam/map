'use strict';

window.Meeting = (function() {

  Meeting.prototype.id = 0;
  Meeting.prototype.reason = '';
  Meeting.prototype.venue = '';
  Meeting.prototype.appointmentDay = '';
  Meeting.prototype.appointmentDayFinished = '';
  // TODO: Implementar en Groovy
  Meeting.prototype.startTime = '';
  Meeting.prototype.endTime = '';
  Meeting.prototype.duration = 0;

  Meeting.prototype.dateSelector = '';
  Meeting.prototype.momentInstance = null;

  function Meeting(moment) {
    this.momentInstance = moment;
    this.startTime = this.momentInstance.format("hh:mm");
    this.endTime = this.momentInstance.add("hours",1).format("hh:mm");
    this.duration = 60;
    this.appointmentDay = this.momentInstance.format("D/MM/YYYY");
  }

  Meeting.prototype.addDuration = function(duration) {
  };
  
  Meeting.createFromJson = function(meetingRaw) {
    var meeting = new Meeting();
    meeting.id = meetingRaw.id;
    meeting.reason = meetingRaw.reason;
    meeting.venue = meetingRaw.venue;
    meeting.appointmentDay = meetingRaw.appointmentDay;
    meeting.appointmentDayFinished = meetingRaw.appointmentDayFinished;
    return meeting;
  };

  return Meeting;

})();