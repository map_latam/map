package com.mymapmanager

enum PrivilegeType {

  READ("privilege.type.read"),
  WRITE("privilege.type.write"),
  EXECUTE("privilege.type.execute")

  private final String code

  PrivilegeType(String code){
    this.code = code
  }

  String getCode(){ return this.code }
}